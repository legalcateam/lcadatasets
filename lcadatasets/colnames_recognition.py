import numpy as np

colnames_instructions = {
    "id": {
        "match": [
            r".*\bid\s.*",
            r"идентификатор\s.*",
            r"uuid",
            r".*_id\b",
            r"\bid\b.*",
            r"№ п/п",
            r"№п/п",
            r"идентификатор",
            r"№",
            r"номер клиента",
            r"idarrears",
            r"crmid",
        ],
        "not match": [
            r".*\bдолжник.*"
        ],
        "dtype_a": "id",
        "default_name": "идентификатор кейса"
    },
    "debtor_id": {
        "match": [
            r".*\bid.*\bдолжник.*",
            r"идентификатор\b.*\bдолжник.*",
            r"clientkey",
            r"id клиента",
        ],
        "not match": [],
        "dtype_a": "id",
        "default_name": "идентификатор должника"
    },
    "debtor_sex": {
        "match": [
            r"\bпол\b.*",
            r".*\bsex\s.*",
            r"[0-9]*_пол",
            r"identity_gender"
        ],
        "not match": [
            r"\bid\b.*"
                ],
        "dtype_a": "factor",
        "default_name": "Пол должника"
    },
    "debtor_birthday": {
        "match": [
            r".*\bдень.*\bрождения\b.*",
            r".*\bдата\sрождения\b.*",
            r".*birthday.*",
            r"[0-9]*_дата\sрождения",
        ],
        "not match": [
            r".*\bпоручител.*",
            r".*контакт.*\bлиц[а, о]\s.*",
            r".*трет.*\bлиц[а, о]\s.*",
            r".*\bродственни\s.*"
        ],
        "dtype_a": "date",
        "default_name": "дата рождения должника"
    },
    "birthday_year": {
        "match": [
            r"birthday_year"
        ],
        "not match": [],
        "dtype_a": "count",
        "default_name": "Дата рождения клиента"
    },
    "credit_number": {
        "match": [
            r".*\bномер\b.*\bдоговора\b.*",
            r".*\bномер\b.*\bзайма\b.*",
            r".*credit_num.*",
            r"№\s.*\bдоговора\b.*",
            r"номер договора",
            r"номер кд",
            r"номер заявки"
        ],
        "not match": [
            r".*\bпоручител.*",
            r".*\bцесси[и, я].*"
        ],
        "dtype_a": "factor",
        "default_name": "Номер кредитного договора"

    },
    "credit_date_created": {
        "match": [
            r".*\bдата\b.*\bдоговора\b.*",
            r".*\bдата\b.*\bзайма\s.*",
            r".*credit_date.*",
            r".*\bдата\b.*\bвыдачи\b.*\bкредит.*",
            r".*\bдата\b.*\bпредост.*\bкредит.*",
            r"VOUCHER_ISSUED".lower(),
            r".*\bдата\b.*\bвыдачи\b.*\bзайм.*",
            r".*\bдата.*\bдолга.*",
            r".*\bдата.*\bначала.*",
            r"дата выдачи",  # по умолчанию - дата выдачи займа, не паспорта. Различать по данным.
            r".*\bдата.*\bзаключ.*\bдоговор.*",
        ],
        "not match": [
            r".*\bпоручител.*",
            r".*\bцесси[и, я]\s.*",
            r".*окончани.*",
            r".*закрыти.*",
            r".*end.*",
            r".*паспорт.*",
            r".*\bзаверш.*",
            r".*\bисполн.*",
            r".*\bреструктуриз.*",
            r".*\bпогашен.*",
            r".*\bпросроч.*",
            r".*\bобразования.*",
            r".*\bудост.*",
            r".*\bдокумент.*"
        ],
        "dtype_a": "date",
        "default_name": "дата выдачи кредита"

    },
    "credit_date_end": {
        "match": [
            r".*\bдата.*\bокончания.*\bдоговора.*",
            r".*\bдата.*\bокончания.*\bзайма.*",
            r".*credit_date_end.*",
            r".*\bдата.*\bокончания.*\bграфика.*",
            r"VOUCHER_DUE".lower(),
            r".*\bдата.*\bзаверш.*\bдоговора.*",
            r".*\bдата.*\bвозврат.*",
            r".*\bдата.*\bисполн.*\bдоговора.*",
            r".*\bдата.*\bпогашен.*\bзайма.*",
            r".*\bдата.*\bокончания.*"
        ],
        "not match": [
            r".*\bпоручител.*",
            r".*\bцесси[и, я]\s.*",
            r".*выдачи.*"
        ],
        "dtype_a": "date",
        "default_name": "плановая (на дату выдачи) дата возврата кредита"

    },
    "credit_sum": {
        "match": [
            r"credit_sum",
            r"22_сумма кредита",
            r".*\bсумма.*\bзайма\b.*",
            r".*\bсумма.*\bкредит.*",
            r".*\bразмер.*\bзайма\b.*",
            r"OUTFLOW_TOTAL".lower(),
            r".*\bсумма\b.*\bвыдачи\b.*",
            r".*\bсумма\b.*\bмикрозайма\b.*",
            r".*\bвыданная.*\bсумма.*",
            r"voucher_principle/ сумма займа",
            r"первоначальная сумма",
            r"сумма договора",
            r"сумма выданного кредита",
            r"сумма предоставленного кредита/кредитный лимит",
            r"запрошенная сумма",
        ],
        "not match": [
            r".*\bосновно.*",
            r".*\bпроцен.*",
            r".*\bкомисс.*",
            r".*\bпени.*",
            r".*\bштраф.*",
            r".*\bплатеж.*",
            r".*\bоплат\b.*",
            r".*\bвсех\b.*",
            r".*\bпоступивш.*",
            r".*\bв\sсчет\sоплаты.*",
            r".*\bтранзакц.*",
            r".*\bвозврат.*",
            r"общая сумма займа"  # МЛБ
            r".*\bVOUCHER_PRINCIPLE.*".lower(),
            r".*\bпоступлени.*",
            r"общая сумма задолженности в валюте займа",
            r".*\bзакрытым\b.*",
        ],
        "dtype_a": "sum",
        "default_name": "Сумма кредита"

    },
    "credit_sum_joint": {  # Общая сумма займа по солидарным должникам в МЛБ
        "match": [
            r"общая сумма займа"  # МЛБ
        ],
        "not match": [
            r".*\bосновно.*",
            r".*\bпроцен.*",
            r".*\bкомисс.*",
            r".*\bпени.*",
            r".*\bштраф.*",
            r".*\bплатеж.*",
            r".*\bоплат\b.*",
            r".*\bвсех\b.*",
            r".*\bпоступивш.*",
            r".*\bв\sсчет\sоплаты.*",
            r".*\bтранзакц.*",
            r".*\bвозврат.*",
        ],
        "dtype_a": "sum",
        "default_name": "Сумма займа"
    },
    "credit_term_days": {
        "match": [
            r".*\bсрок\s+.*кредита.*",
            r".*\bсрок\s+.*займ.*",
            r"[0-9]*_срок\sдоговора\s\(дни\)",
            r"INITIALDURATION".lower(),
            r".*\bсрок.*\b(дней).*",
            r".*\bсроки\b.*\bпольз.*\bзайм.*",
            r"срок кредита в днях"
        ],
        "not match": [
            r".*\bпросроч.*",
            r".*мес.*"
        ],
        "dtype_a": "count",
        "default_name": "Срок действия кредитного договора(в днях)"

    },
    "credit_term_months": {
        "match": [
            r".*\bсрок.*\b(мес.).*",
            r".*рок.*\bкредит.*\(мес.*",
        ],
        "not match": [
            r".*\bсрок.*\b(дней).*"
        ],
        "dtype_a": "count",
        "default_name": "Срок действия кредитного договора(в месяцах)"

    },
    "credit_rate": {
        "match": [
            r".*\bставка.*",
            r"[0-9]*_ставка по кредиту",
            r"%",
            r"размер\sосновного\sпроцент.*"
        ],
        "not match": [
            r".*\bнеустойк.*",
            r".*\bштраф.*",
            r".*\bпен.*",
            r".*\bрезервирован.*",
            r".*\bнач.*",
            r".*\bслучайн.*"
        ],
        "dtype_a": "other_numeric",
        "default_name": "ставка по кредиту"

    },

    "debt_principal": {
        "match": [
            r"\bод\b",
            r"основной долг.*",
            r".*principal.*",
            r"остаток\b.*\bзадолженности\b.*\bод\s.*",
            r".*остаток\b.*од\b.*",
            r"482_основной долг",
            r".*сумма\b.*основного\sдолга\b.*",
            r".*cумма\b.*основного\sдолга\b.*",
            r".*сумма\b.*\bпо\b.*\bосновному\sдолгу\b.*",
            r"VOUCHER_PRINCIPLE".lower(),
            r".*\bзадолженность\b.*\bод\b.*",
            r".*\bсумма\b.*\bод\b.*",
            r".*\bзадолженность\b.*\bобщая\b.*",
            r".*\bдолг\b.*\bпо\b.*\bод\b.*",
            r"текущий основной долг",
            "сумма ос",  # МЛБ
            r"сумма задолженности по од",
            r"просроченный од",
            r"задолженность по основному долгу в валюте займа",
            r"тело кредита"
            r"Сумма од - итого (в валюте кредита)",
            r"сумма просроченного долга"
        ],
        "not match": [
            r".*\bитого.*",
            r".*%.*",
            r"общая сумма ос",  # МЛБ
            r"общая сумма задолженности",
            r".*\bнач"

        ],
        "dtype_a": "sum",
        "default_name": "сумма основного долга"

    },

    "debt_principal_joint": {  # Общая сумма ОД по солидарным должникам в МЛБ
        "match": [
            r"общая сумма ос"  # МЛБ
        ],
        "not match": [
            r".*\bитого.*",
            r".*%.*",
        ],
        "dtype_a": "sum",
        "default_name": "Основной долг"

    },
    "debt_interest": {
        "match": [
            r"проценты.*",
            r"остаток\b.*\bзадолженности\b.*\%.*",
            r"483_проценты",
            r"491_проценты\sпросроченные",
            r".*сумма\b.*процентов\b.*",
            r".*cумма\b.*процентов\b.*",
            r".*сумма\b.*\bпо\b.*\bпроцентам\b.*",
            r"LATE_FEES_TOTAL".lower(),
            r"fees_total",
            r"VOUCHER_COMMISSION".lower(),
            r".*\bзадолженность\b.*\bпроцент.*",
            r".*\bпросрочен.*\s%.*",
            r"%",
            r".*\bостаток.*\bпроцент.*",
            r".*\bдолг.*\bпроцент.*",
            r".*\bзадолженность.*\s%.*",
            r"текущие % за просрочку",
            r"текущие %",
            r".*\bтекущие\b.*\bпроценты\b.*\bна\b.*\bпросроченный\b.*\bОД\b.*",
            r".*\bтекущие\b.*\b%\b.*\bна\b.*\bпросроченный\b.*\bод\b.*",
            r".*\bОстаток срочной задолженности по %%\b.*".lower()
        ],
        "not match": [
            r".*ставк.*",
            r".*итого.*",
            r".*\bсумма\b.*\bзайма\b.*",
            r".*\bпени.*",
            r"общая сумма процентов",  # МЛБ
            r".*\bнач"  # КИР
        ],
        "dtype_a": "sum",
        "default_name": "сумма процентов"

    },
    "debt_interest_joint": {  # Общая сумма процентов по солидарным должникам в МЛБ
        "match": [
            r"общая сумма процентов",  # МЛБ
        ],
        "not match": [
            r".*ставк.*",
            r".*итого.*",
            r".*\bсумма\b.*\bзайма\b.*",
            r".*\bпени.*",
        ],
        "dtype_a": "sum",
        "default_name": "Проценты"
    },

    "debt_comissions": {
        "match": [
            r".*\bкомис.*",
            r".*\bкоммис.*",
        ],
        "not match": [
            r".*ставк.*",
            r".*итого.*",
            r".*\bсумма\b.*\bзайма\b.*",
            r".*\bсписанны.*",
            r".*\bнач",
            r".*\bдата\b.*"
        ],
        "dtype_a": "sum",
        "default_name": "сумма комиссий"

    },
    "debt_penalties": {
        "match": [
            r".*\bпени.*",
            r".*\bштраф.*",
            r".*\bнеустойк.*",
            r".*\bpenalt.*",
            r"485_пени"
        ],
        "not match": [
            r".*ставк.*",
            r".*итого.*",
            r".*%.*",
            r".*\bсумма\b.*\bзайма\b.*",
            r".*\bнач",
            r".умма\sоплат.*",
            r"размер.*\bпроцента",
            r".*\bсрок.*"
        ],
        "dtype_a": "sum",
        "default_name": "сумма Пени/штрафы/неустойки"

    },
    "debt_duty": {
        "match": [
            r"гп",
            r".*\bгос.*\bпош.*",
            r".*\bгоспошлин.*"
        ],
        "not match": [],
        "dtype_a": "sum",
        "default_name": "сумма госпошлины"

    },
    "dpd": {
        "match": [
            r".*\bдней.*\bпросрочки.*",
            r"дней просрочки",
            r".*\bдней\sпз\b.*",
            r"num_срок_просрочки_с_даты_начала_работы",
            r".*\bдней\sпз_.*",
            r".*\bдн.*\s\bпросроч.*",
            r"DAYS_SINCE_DUE_AT".lower(),
            r".*\bсрок.*\bпросроч.*",
            r".*\bсрок.*\bзадолж.*",
            r".*\bdpd.*",
            r".*\bпросрочк.*\bдн.*"
        ],
        "not match": [],
        "dtype_a": "count",
        "default_name": "Срок просрочки"

    },
    "overdue_date": {
        "match": [
            r".*\bдата.*\bпросроч.*",
            r"21_дата\sначала\sпросрочки",
            r"DUEAT".lower(),
            r"дата образования задолженности"
        ],
        "not match": [
            r".*выдачи.*",
            # r"дата первой просрочки "
            # Это зачем здесь? Дата первой просрочки - это же и есть overdue_date - 2020-06-25 А.Голобородько
        ],
        "dtype_a": "date",
        "default_name": "Дата выхода на просрочку"

    },
    "debt_other": {
        "match": [
            r"представительские услуги",
            r"издержки по процедуре банкротства.*",
            r"debt_other",
            r"прочее",
            r".*\bпрочи.*\bрасход.*"
        ],
        "not match": [],
        "dtype_a": "sum",
        "default_name": "сумма прочих составляющих задолженности"

    },
    "b_has_judical_writ": {
        "match": [
            r".*\bрешени[я, е]\sсуда.*",
            r".*\bсуд.*\s\bрешен.*",
            r".*\bисполнительн.*\bдокумент.*",
            r".*\bприказ.*",
            r".*\bполучен.*\bид\b.*",
            r"суд",
            r".*\bисполнительный лист\b.*"
        ],
        "not match": [
            r".*\bграфик.*",
            r".*\bплатеж.*",
            r".*\bдата.*",
            r".*\bотмена.*",
            r".*\bсудебное.*"
        ],
        "dtype_a": "factor",
        "default_name": "отметка о наличии исполнительного документа (на дату `creation_date`)"

    },
    "payments_holdhistory_sum": {
        "match": [
            r".*\bсумма.*\s.*платеже.*",
            r"234_сумма\b.*\bпоступившая\b.*\bот\b.*\bдолжника\b.*",
            r".*\bсумма\sпогашений\b.*",
            r".*\bсумма\b.*\bпоступившая\b.*\bот\b.*\bдолжника\b.*",
            r"INFLOW_TOTAL.*".lower(),
            r".*\bвсего.*\bоплачено.*",
            r".*\bсумма\b.*\bоплат\b.*",
            r".*\bоплачено.*",
            r"сумма поступлений на р/сч",
            r"сумма платежей (на дату `creation_date`)"

        ],
        "not match": [
            r".*\bк\sоплате\b.*",
            r".*\bпоследн.*",
            r".*\bплатам.*",
            r".*\bигнорировать.*"
        ],
        "dtype_a": "sum",
        "default_name": "Сумма поступившиих платежей"

    },
    "payments_holdhistory_cnt": {
        "match": [
            r".*\bколичество.*\bплатежей.*",
            r".*\bчисло.*\bплатежей.*",
            r"[0-9]*_количество.*\bплатежей.*",
            r".*\bколичество\sпогашений\b.*",
            r".*\bкол-во.*\bплатежей.*",
            r"NUM_IN_PAYMENTS".lower(),
            r".*\bкол-во\b.*\bвыплат\b.*"

        ],
        "not match": [
            r".*\bпоследн.*"
        ],
        "dtype_a": "count",
        "default_name": "количество платежей (на дату `creation_date`)"

    },
    "debt_sum_original": {
        "match": [
            r".*\bзадолженность.*\bитого.*",
            r".*\bитого.*\bзадолженност.*",
            r"481_сумма",
            r".*\bобщая\sсумма\sзадолженности\b.*",
            r"VOUCHER_TOTAL_AMOUNT_OWED".lower(),
            r".*\bобщая\b.*\bзадолженность\b.*",
            r".*\bсумма\b.*\bдолга.*",
            r".*\объём.*\bуступ.*\bправ.*",
            r".*\bсумм.*\bзадолженност.*",
            r".*\bполный\sдолг\b.*",
            r".*.*\bзадолженность.*\bсумма.*",
            r"общая сумма задолженности",
            r"осз",
            r".*\bтекущая.*\bзадолженость.*",
            r"сз тек",
            r".*\bсостояние задолженности: итого\b.*"
        ],
        "not match": [
            r".*\bграфик.*",
            r".*\bгп\b.*",
            r".*\bгос.*\bпош.*",
            r".*\bгоспош.*",
            r".*\bпени.*",
            r".*\bштраф.*",
            r".*\bнеустойк.*",
            r".*\bpenalt.*",
            r".*\bосновно.*",
            r".*\bпроцент.*",
            r".*\bплатеж.*",
            r".*\bоплат\b.*",
            r".*\bпоступившая.*",
            r".*\bпросроч.*",
            r".*\bнач"

        ],
        "dtype_a": "sum",
        "default_name": "Общая сумма долга"

    },

    "debt_sum_original_joint": {  # Общая сумма долга по солидарным должникам в МЛБ
        "match": [
            #r"общая сумма задолженности",  # МЛБ
            # временно убираю
        ],
        "not match": [
            r".*\bграфик.*",
            r".*\bгп\b.*",
            r".*\bгос.*\bпош.*",
            r".*\bгоспош.*",
            r".*\bпени.*",
            r".*\bштраф.*",
            r".*\bнеустойк.*",
            r".*\bpenalt.*",
            r".*\bосновно.*",
            r".*\bпроцент.*",
            r".*\bплатеж.*",
            r".*\bоплат\b.*",
            r".*\bпоступившая.*",
        ],
        "dtype_a": "sum",
        "default_name": "Общая сумма долга"

    },

    "repayment_sum": {
        "match": [
            r"сумма к возврату"
        ],
        "not match": [],
        "dtype_a": "sum",
        "default_name": "Сумма к возврату"
    },

    "effective_judical_date": {
        "match": [
            r".*\bдата\sвступления\sв\sзаконную\sсилу\b.*",
        ],
        "not match": [
        ],
        "dtype_a": "date",
        "default_name": "дата вступления в законную силу исполнительного документа"
    },

    "court_decision_date": {
        "match": [
            r".*\bдата\sрешения\sсуда\b.*",
        ],
        "not match": [
        ],
        "dtype_a": "date",
        "default_name": "Дата решения суда"
    },

    "court_filing_date": {
        "match": [
            r".*\bдата\sподачи\s.*\sсуд\b.*",
        ],
        "not match": [
        ],
        "dtype_a": "date",
        "default_name": "Дата подачи в суд"
    },

    "b_approp": {
        "match": [
            "кейс пригоден к работе"
        ],
        "not match": [],
        "dtype_a": "bool",
        "default_name": "Кейс пригоден к работе"
    },
    "b_has_docs": {
        "match": [
            "has_docs"
        ],
        "not match": [],
        "dtype_a": "bool",
        "default_name": "отметка о наличии долгового досье"

    },

    "b_has_passport": {
        "match": [
            r"b_has_passport",
            r".*\bналичи.*\bпаспорт.*"
        ],
        "not match": [
            r".*\bотсутств.*"
        ],
        "dtype_a": "bool",
        "default_name": "отметка о наличии скана паспорта"
    },
    "b_has_offer": {
        "match": [
            r"b_has_passport",
            r".*\bналичи.*\bоферт.*"
        ],
        "not match": [
            r".*\bотсутств.*"
        ],
        "dtype_a": "bool",
        "default_name": "отметка о наличии скана оферты"
    },
    "b_bankrupcy": {
        "match": [
            r"bankrupcy",
            r"[0-9]*_банкротство",
            r".*\bбанкрот.*"
        ],
        "not match": [],
        "dtype_a": "bool",
        "default_name": "отметка о банкротстве должника"

    },
    "b_fraud": {
        "match": [
            r".*\bмошенни.*",
        ],
        "not match": [],
        "dtype_a": "bool",
        "default_name": "Установлен факт мошенничества"

    },
    "b_act_of_impossibility": {
        "match": [
            "act_of_impossibility",
            r".*\bпостановление\b.*\bокончании\b.*\bневозм.*",
            r".\bакт.* о невозможности\b.*"
        ],
        "not match": [],
        "dtype_a": "bool",
        "default_name": "отметка о наличии акта о невозможности взыскания"

    },
    "b_death": {
        "match": [
            "death",
            r".*\bфакт.*\bсмерт.*",
            r".*\bсмерт.*"
        ],
        "not match": [],
        "dtype_a": "bool",
        "default_name": "отметка о смерти должника"

    },
    "date_workstart": {
        "match": [
            "date_начало_работы",
            r"дата_начала_работы"
        ],
        "not match": [],
        "dtype_a": "date",
        "default_name": "Дата начала работы"

    },
    "reg_region_code": {
        "match": [
            "[0-9]*_код региона"
        ],
        "not match": [],
        "dtype_a": "other_numeric",
        "default_name": "код региона регистрации (целое число)"

    },
    "exec_date": {
        "match": [
            r"date_дата_ип",
            r".*\bдата\b.*\bвозбуждения\b.*\bип\b.*",
            r".*\bдата\b.*\bвозбуждения\b.*\bиспол.*"
        ],
        "not match": [],
        "dtype_a": "date",
        "default_name": "дата начала исполнительного производства"

    },

    "exec_end_date": {
        "match": [
            r".*\bдата\b.*\bокончания\b.*\bип\b.*",
            r".*\bдата\b.*\bокончания\b.*\bиспол.*",
            r".*\bдата\b.*\bзавершения\b.*\bип\b.*",
            r".*\bдата\b.*\bзавершения\b.*\bиспол.*"
        ],
        "not match": [],
        "dtype_a": "date",
        "default_name": "Дата окончания ИП"

    },

    "collateral_type": {
        "match": [
            r".*\bвид\b.*\bзалог\b.*",
            r".*\bтип\b.*\bзалог\b.*",
        ],
        "not match": [],
        "dtype_a": "date",
        "default_name": "Вид залогового имущества"
    },

    "lastpayment_date": {
        "match": [
            r"272_дата\sпоследнего\sплатежа",
            r".*\bдата.*\bпоследнего.*\bплатежа",
            r".*\bпоследн.*\bдата.*\s.*плат",
            r"last_payment_inflow"
        ],
        "not match": [
            r"last_payment_inflow_amount"
                ],
        "dtype_a": "date",
        "default_name": "дата последнего платежа",

    },
    "lastpayment_sum": {
        "match": [
            r"276_сумма\sпоследнего\sплатежа",
            r".*\bсумма.*\bпоследнего.*\bплатежа"
        ],
        "not match": [],
        "dtype_a": "sum",
        "default_name": "Сумма последнего платежа"

    },
    "portfolio": {
        "match": [
            "fctr_портфель",
        ],
        "not match": [],
        "dtype_a": "factor",
        "default_name": "Реестр / лот"

    },
    "debtor_surname": {
        "match": [
            r"\bфамилия\b.*"
        ],
        "not match": [],
        "dtype_a": "char",
        "default_name": "Фамилия"

    },
    "debtor_name": {
        "match": [
            r"\bимя\b.*"
        ],
        "not match": [],
        "dtype_a": "char",
        "default_name": "Имя"

    },
    "debtor_patronym": {
        "match": [
            r"\bотчество\b.*",
            r".*\bотчетство\b.*"  # учит. опечатку
        ],
        "not match": [],
        "dtype_a": "char",
        "default_name": "Отчество"

    },
    "debtor_fio": {
        "match": [
            r"\bфио\b.*",
            r"\bзаемщик\b"
        ],
        "not match": [
            r"\bколлектор.*"
        ],
        "dtype_a": "char",
        "default_name": "ФИО"

    },
    "debtor_birthplace": {
        "match": [
            r"\bместо\sрождения\b.*"
        ],
        "not match": [],
        "dtype_a": "char",
        "default_name": "Место рождения должника"

    },
    "debtor_passport_whole_data": {
        "match": [
            r"\bпаспортные\sданные\b.*"
        ],
        "not match": [],
        "dtype_a": "char",
        "default_name": "Паспортные данные"

    },
    "debtor_passport_date_issue": {
        "match": [
            r"\bдата\sвыдачи\sпаспорта\b.*",
            r"\bдата\sвыдачи\sдокумента\b.*",
            # r"дата\b.*\bвыдачи\b.*"
            # По умолчанию дата выдачи - это дата выдачи кредита. Различать по разбросу дат
            # Дата выдачи кредита более однородна и разброс меньше, чем дата выдачи паспорта
        ],
        "not match": [
            r".*\bданные\b.*",
            r".*\bкредит.*",
            r".*\bзайм.*"
        ],
        "dtype_a": "date",
        "default_name": "Дата выдачи паспорта"

    },
    "debtor_passport_type": {
        "match": [
            r"\bкод\sпаспорта\b.*",
            r"\bкод\sдокумента\b.*",
        ],
        "not match": [
            r".*\bданные\b.*",
            r".*\bкредит.*",
            r".*\bзайм.*",
            r".*\bподраздел.*",
            r".*\bпоручител.*"
        ],
        "dtype_a": "char",
        "default_name": "Тип документа"
    },
    "debtor_passport_serial": {
        "match": [
            r"\bсерия\s.*паспорта\b.*",
            r"\bсерия\s.*документа\b.*"
        ],
        "not match": [
            r".*\bданные\b.*",
            r".*\bкредит.*",
            r".*\bзайм.*",
            r".*\bномер.*",
        ],
        "dtype_a": "date",
        "default_name": "Дата выдачи паспорта"
    },
    "debtor_passport_number": {
        "match": [
            r"\bномер\s.*паспорта\b.*",
            r"\bномер\s.*документа\b.*"
        ],
        "not match": [
            r".*\bданные\b.*",
            r".*\bкредит.*",
            r".*\bзайм.*",
        ],
        "dtype_a": "date",
        "default_name": "Дата выдачи паспорта"
    },
    "credit_type": {
        "match": [
            r".*\bпродукт.*",
            r".*\bтип\b.*\bкредит.*",
            r".*\bтип\b.*\bзайм.*",
            r".*\bвид\b.*\bкредит.*",
            r".*\bвид\b.*\bзайм.*",
            r".*\bтип\b.*\bпроекта.*"
        ],
        "not match": [
            r".*\bнаименован.*",
            r".*\bкомисси.*",
        ],
        "dtype_a": "factor",
        "default_name": "тип выданного кредита *(напр., CARD или CASH. Наименования зафиксированы в учетной системе)*"

    },
    "credit_product_name": {
        "match": [
            r".*\bнаименован.*\bпродукт.*",
            r".*\bнаименован.*\bуслуги.*"
        ],
        "not match": [],
        "dtype_a": "factor",
        "default_name": "Наименование кредитного продукта *(произвольное наименование, принятое у кредитора)*"

    },
    "credit_currency": {
        "match": [
            r".*\bвалют.*",
            r"вал.*",
            r".*\bвал."
        ],
        "not match": [
            r".*\bсумма.*",
            r".*\bосновно.*\bдолг",
            r".*\bпроцент.*",
            r".*\bпени.*",
            r".*\bштраф.*",
            r".*\bовердрафт.*",
            r".*\bнеустой.*",
            r".*\bкомисси.*"
        ],
        "dtype_a": "factor",
        "default_name": "валюта кредита"

    },
    "credit_issue_region": {
        "match": [
            r"\bрегион.*\bвыдачи\b",
            r"REGION".lower(),
            r"регион",
            r"область",
        ],
        "not match": [
            r".*\bпрожив.*",
            r".*\bрегистрац.*",
            r".*\bфактич.*",
            r".*\bработ.*"
        ],
        "dtype_a": "factor",
        "default_name": "номер региона, в котором был выдан кредит (целое число)"
    },
    "debtor_address_fact": {
        "match": [
            r"\bадрес.*\bфакт.*",
            r".*\bфакт.*\bадрес.*",
        ],
        "not match": [
            r"\bтелефон.*"
        ],
        "dtype_a": "char",
        "default_name": "Адрес проживания заемщика"

    },
    "debtor_phone_home_reg": {
        "match": [
            r".*\bтелефон.*\bрег.*",
        ],
        "not match": [],
        "dtype_a": "char",
        "default_name": "Телефон по адресу регистрации заемщика"

    },
    "debtor_phone_home_fact": {
        "match": [
            r".*\bтелефон.*\bфакт.*",
        ],
        "not match": [],
        "dtype_a": "char",
        "default_name": "Телефон по адресу проживания заемщика"

    },
    "b_debtor_phone_job": {
        "match": [
            r".*\bтелефон.*\bрабо.*",
            r".*\bрабо.*\bтелефон.*"
        ],
        "not match": [],
        "dtype_a": "char",
        "default_name": "признак наличия рабочего телефона"

    },
    "b_debtor_phone_mobile": {
        "match": [
            r".*\bтелефон.*\bмоб.*",
            r".*\bмоб.*\bтелефон.*",
            r".*\bтелефон.*",  # если написано просто "телефон", будем считать, что это мобильный
        ],
        "not match": [
            r".*\bрабо.*",
            r".*\bфакт.*",
            r".*\bрег.*",
            r".*\bдомаш.*",
            r".*\bадрес.*",
            r".*\bпрожив.*",
        ],
        "dtype_a": "char",
        "default_name": "признак наличия мобильного телефона"

    },
    "debtor_phone_third_party": {
        "match": [
            r".*\bтелефон.*\bконтакт.*\bлиц.*",
        ],
        "not match": [],
        "dtype_a": "char",
        "default_name": "признак наличия телефона, принадлежащего третьему лицу"

    },
    "debtor_phones_cnt": {
        "match": [
            r"\bколич.*\bтелеф.*",
        ],
        "not match": [],
        "dtype_a": "char",
        "default_name": "Количество телефонов"
    },
    "debtor_job_organization_name": {
        "match": [
            r".*\bнаименован.*\bорганизац.*",
        ],
        "not match": [
            r".*\bкредит.*"
        ],
        "dtype_a": "char",
        "default_name": "Место работы: наименование организации"

    },
    "b_debtor_job_address": {
        "match": [
            r".*\bадрес.*\bрабо.*",
        ],
        "not match": [
            r".*\bкредит.*"
            r".*\факт.*"
        ],
        "dtype_a": "char",
        "default_name": "признак наличия информации об адресе места работы"

    },
    "debtor_job_region": {
        "match": [
            r".*\bрегион.*\bрег.*",
            r".*\bсубъект.*\bрегистрац.*"
        ],
        "not match": [
            r".*\bкредит.*"
        ],
        "dtype_a": "char",
        "default_name": "регион адреса места работы"

    },
    "debtor_region_fact": {
        "match": [
            r".*\bгород.*\bпрожив.*",
            r".*\bрегион.*\bпрожив.*"
        ],
        "not match": [
            r".*\bкредит.*"
        ],
        "dtype_a": "char",
        "default_name": "регион проживания должника (текст)"

    },

    "credit_regular_payment_amount": {
        "match": [
            r".*\bразмер.*\bплат.*",
            r".*\bсумма\b.*\bплатежа.*"
        ],
        "not match": [
            r".*\bпоследн.*"
        ],
        "dtype_a": "sum",
        "default_name": "Сумма платежа по кредиту"

    },
    "debtor_has_exec": {
        "match": [
            r".*\bналичие.*\bип.*",
        ],
        "not match": [],
        "dtype_a": "factor",
        "default_name": "Наличие исполнительного производства"

    },
    "debtor_children_cnt": {
        "match": [
            r".*\bколичество.*\bдетей.*",
            r".*\bкол-во.*\bдетей.*",

        ],
        "not match": [],
        "dtype_a": "count",
        "default_name": "Количество детей"

    },
    "debtor_job_position": {
        "match": [
            r".*\bдолжность\b.*",
            r".*\bposition\b.*"
        ],
        "not match": [],
        "dtype_a": "char",
        "default_name": "должность должника"

    },
    "debtor_education_status": {
        "match": [
            r".*\bобразование\b.*",
        ],
        "not match": [],
        "dtype_a": "char",
        "default_name": "Образование"

    },
    "debtor_marital_status": {
        "match": [
            r".*\bматрим.*\bстатус\b.*",
            r".*\bсемейн.*\bположен.*",
            r"marital_status"
        ],
        "not match": [],
        "dtype_a": "factor",
        "default_name": "семейное положение должника"

    },
    "debtor_monthly_income": {
        "match": [
            r".*\bдоход\b.*\bдолжника\b.*",
            r".*\bзарплата\b.*",
            r".*\bзар.*\bплата\b.*",
        ],
        "not match": [],
        "dtype_a": "sum",
        "default_name": "доход должника в месяц"

    },
    "credit_total_cost_for_debtor": {
        "match": [
            r".*\bстоимость\b.*\bкредита\b.*",
            r"пск"
        ],
        "not match": [
            r".*\bнеустойк.*",
            r".*\bштраф.*",
            r".*\bпен.*",
            r".*\bставка.*",
        ],
        "dtype_a": "other_numeric",
        "default_name": "полная стоимость кредита (по договору)"

    },
    "debt_issue_format": {
        "match": [
            r".*\bформа\b.*\bпредоставления.*",
            r".*\bканал.*\bвыдач.*"
        ],
        "not match": [],
        "dtype_a": "factor",
        "default_name": "формат выдачи займа *(напр., онлайн / наличные)*"

    },
    "penalty_rate": {
        "match": [
            r".*\b%\b.*\bпени\b.*",
            r"% пени",
            r"Размер штрафного процента"
        ],
        "not match": [],
        "dtype_a": "other_numeric",
        "default_name": "ставка пени"

    },
    "age": {
        "match": [
            r".*\bвозраст\b.*",
            r".*\bage\b.*",

        ],
        "not match": [
            r".*\bкредит.*"
        ],
        "dtype_a": "count",
        "default_name": "Возраст заемщика"
    },
    'collateral': {
        "match": [
            r".*\bобеспечение\b.*",
            r".*\bобеспечения\b.*"
        ],
        "not match": [
        ],
        "dtype_a": "factor",
        "default_name": "Обеспечение"

    },
    'b_exec_ended': {
        "match": [
            r".*\bпостановлен.*\bоконч.*\bип.*",
            r".*\bип\sокончено\b.*"
        ],
        "not match": [
        ],
        "dtype_a": "bool",
        "default_name": "признак наличия оконченного исполнительного производства"

    },
    'b_has_collateral': {
        "match": [
            r".*\bналич.*\bобеспечен.*",
            r".*\bесть.*\bобеспечен.*",
        ],
        "not match": [
        ],
        "dtype_a": "bool",
        "default_name": "признак наличия обеспечения"
    },
    "debt_status": {
        "match": [
            r".*\bстадия.*",
            r".*\bстатус\b.*",
            r".*\bэтап.*"
        ],
        "not match": [
            r".*\bматрим.*"
        ],
        "dtype_a": "factor",
        "default_name": "текущий статус кейса (текст, по данным клиента/цессионария)"
    },
    "b_medical_status_debtor_is_disabled": {
        "match": [
            r".*\bинвалид.*"
        ],
        "not match": [],
        "dtype_a": "bool",
        "default_name": "признак наличия инвалидности у должника"
    },
    "debtor_email": {
        "match": [
            r".*\bemail.*",
            r".*\bэлектрон.*\bпочт.*"
        ],
        "not match": [],
        "dtype_a": "char",
        "default_name": "Email должника"
    },
    "previous_debts_cnt": {
        "match": [
            r".*\bколичеств.*\bпредыдущ.*\bзайм.*",
        ],
        "not match": [],
        "dtype_a": "char",
        "default_name": "количество займов, выданных должнику ранее тем же кредитором"
    },
    "date_of_relevance": {
        "match": [
            r".*\bдата.*\bактуальност.*",
            r".*\bдата.*\bреестр.*",
        ],
        "not match": [
            r".*\bвыдач.*",
            r".*\bоконч.*",
            r".*\bвозвр.*",
            r".*\bзакрыт.*",
        ],
        "dtype_a": "date",
        "default_name": "Дата актуальности"
    },
    "lot": {
        "match": [
            r".*\bлот.*",
            r".*\bреестр.*"
        ],
        "not match": [
            r".*\bлотере.*",
            r".*\bдата.*"
        ],
        "dtype_a": "factor",
        "default_name": "Лот"
    },
    "creditor": {
        "match": [
            r".*\bкредитор.*",
            r"кому должен"
        ],
        "not match": [
            r".*\bзадолжен.*",
            r".*\bсумм.*",
            r".*\bявляетс.*",
            r".*\bдата.*",
            r".*\bстадия.*",
            r".*\bсудебное.*",
            r".*\bисполнительн.*",
            r".*\bиск.*"

        ],
        "dtype_a": "factor",
        "default_name": "наименование кредитора"

    },
    "washes_cnt": {
        "match": [
            r".*\bразмещ.*\bколлектор.*",
            r".*\bколичест.*\bка\b.*"
        ],
        "not match": [
            r".*\bзадолжен.*",
            r".*\bсумм.*",
            r".*\bфакт.*",
        ],
        "dtype_a": "count",
        "default_name": "количество размещений в других коллекторских агенствах"

    },

    "b_washed": {
        "match": [
            r".*\bразмещ.*\bколлектор.*",
            r".*\bфакт.*\bколлект.*",
            r".*\bпередавалось.*\bагентство.*",
            r".*\bколлекторск.*\bагентств.*"
        ],
        "not match": [
            r".*\bзадолжен.*",
            r".*\bсумм.*",
            r".*\bколичеств.*",
            r".*\bчисл.*",
        ],
        "dtype_a": "bool",
        "default_name": "Факт размещений в коллекторских организациях"
    },

    "lastcontact_date": {
        "match": [
            r".*\bдата.*\bконтакт.*"
        ],
        "not match": [
            r".*\bзадолжен.*",
            r".*\bсумм.*",
            r".*\bтрет.*\bлиц.*"
        ],
        "dtype_a": "date",
        "default_name": "Дата последнего контакта с должником"
    },
    "lastcontact_result": {
        "match": [
            r".*\bрезульт.*\bконтакт.*"
        ],
        "not match": [
            r".*\bзадолжен.*",
            r".*\bсумм.*",
            r".*\bтрет.*\bлиц.*"
        ],
        "dtype_a": "factor",
        "default_name": "Результаты последнего контакта с должником"
    },
    'b_contacts_refusal': {
        "match": [
            r".*\bотказ.*\bот.*\bвзаимодейств.*",
            r"признак наличия отказа от взаимодействия"
            r"признак наличия отзыва согласия на иные способы взаимодействия"
        ],
        "not match": [
        ],
        "dtype_a": "bool",
        "default_name": "признак наличия отказа от взаимодействия"
    },
    'claim_sum': {
        "match": [
            r"claim_sum",
            r"сумма исковых требований"
        ],
        "not match": [
        ],
        "dtype_a": "sum",
        "default_name": "Сумма исковых требований"
    },
    'claim_interest': {
        "match": [
            r"claim_interest",
            r"сумма процентов для расчета исковых требований"
        ],
        "not match": [
        ],
        "dtype_a": "sum",
        "default_name": "Сумма процентов для расчета исковых требований"
    },
    'claim_calc_type': {
        "match": [
            r"cпособ расчета исковых требований",
            r"claim_calc_type"
        ],
        "not match": [
        ],
        "dtype_a": "factor",
        "default_name": "Способ расчета исковых требований"
    },
    "payment_6_m_ago": {
        "match": [
            r".*\bплатеж\b.*\b6\b.*\bмес\b.*\bназад\b.*"
        ],
        "not match": [],
        "dtype_a": "count",
        "default_name": "Платеж 6 мес назад"
   },
   "payment_5_m_ago": {
        "match": [
            r".*\bплатеж\b.*\b5\b.*\bмес\b.*\bназад\b.*"
        ],
        "not match": [],
        "dtype_a": "count",
        "default_name": "Платеж 5 мес назад"
   },
   "payment_4_m_ago": {
        "match": [
            r".*\bплатеж\b.*\b4\b.*\bмес\b.*\bназад\b.*"
        ],
        "not match": [],
        "dtype_a": "count",
        "default_name": "Платеж 4 мес назад"
   },
    "payment_3_m_ago": {
        "match": [
            r".*\bплатеж\b.*\b3\b.*\bмес\b.*\bназад\b.*"
        ],
        "not match": [],
        "dtype_a": "count",
        "default_name": "Платеж 3 мес назад"
   },
    "payment_2_m_ago": {
        "match": [
            r".*\bплатеж\b.*\b2\b.*\bмес\b.*\bназад\b.*"
        ],
        "not match": [],
        "dtype_a": "count",
        "default_name": "Платеж 2 мес назад"
   },
    "payment_1_m_ago": {
        "match": [
            r".*\bплатеж\b.*\b1\b.*\bмес\b.*\bназад\b.*"
        ],
        "not match": [],
        "dtype_a": "count",
        "default_name": "Платеж 1 мес назад"
   },
   "reg_zip": {
        "match": [
            r".*\bиндекс\b.*\bпрописки\b.*"
        ],
        "not match": [
            r".*\bresidance\b.*\bzipcode\b.*",
            r".*\bиндекс\b.*\bпроживания\b.*"
        ],
        "dtype_a": "char",
        "default_name": "Индекс прописки"
   },
   "fact_zip": {
        "match": [
            r".*\bresidance\b.*\bzipcode\b.*",
            r".*\bиндекс\b.*\bпроживания\b.*",
            r".*\bиндекс\sпроживания\b.*"

        ],
        "not match": [
            r".*\bиндекс\b.*\bпрописки\b.*",
            r".*\bregister\b.*\bindex\b.*"
        ],
        "dtype_a": "char",
        "default_name": "Индекс проживания"
   },
   "payment_in_month": {
        "match": [
             r".*\bплатеж\b.*\bв\b.*\bэтом\b.*\bмес\b.*",
             r".*\bплатеж\b.*\bв\b.*\bтекущем\b.*\bмесяце\b.*"
        ],
        "not match": [],
        "dtype_a": "sum",
        "default_name": "Платеж в этом месяце"
    },

    "planned_closing_date": {
        "match": [
             r".*\bплановая\b.*\bдата\b.*\bзакрытия\b.*"
        ],
        "not match": [],
        "dtype_a": "date",
        "default_name": "Плановая дата закрытия"
    },
    "report_date": {
        "match": [
             r".*\bдата\sотчета\b.*"
        ],
        "not match": [],
        "dtype_a": "date",
        "default_name": "Дата отчета"
    },
    "first_apl": {
        "match": [
            r".*\bfirstapplication\b.*",
            r".*\bfirst\b.*\bapplication\b.*"
        ],
        "not match": [],
        "dtype_a": "bool",
        "default_name": "Первое обращение за кредитом"
    },
    "pesel": {
        "match": [
            r".*\bpesel\b.*"
        ],
        "not match": [],
        "dtype_a": "other_numeric",
        "default_name": "Идентификационный код личности "
    },
    "fact_zipcode": {
        "match": [
            r"baddress_zipcode",
            r".*\baddress\b.*\bzipcode\b.*",
        ],
        "not match": [
            r"work_address_zipcode"
                ],
        "dtype_a": "other_numeric",
        "default_name": "Индекс проживания"
    },
    "work_adr_zipcode": {
        "match": [
            r"work_address_zipcode",
            r".*\bwork\b.*\baddress\b.*\bzipcode\b.*"
        ],
        "not match": [],
        "dtype_a": "other_numeric",
        "default_name": "Рабочий индекс"
    },
    "debtor_region_work": {
        "match": [
            r"work_region",
            r".*\bwork\b.*\bregion\b.*"
        ],
        "not match": [],
        "dtype_a": "char",
        "default_name": "Рабочий регион"
    },
    "promo_amount": {
        "match": [
            r"promo_amount",
            r".*\bpromo\b.*\bamount\b.*"
        ],
        "not match": [],
        "dtype_a": "sum",
        "default_name": "Сумма акций"
    },
    "inflow_prolong": {
        "match": [
            r"inflow_prolongation",
            r".*\binflow\b.*\bprolongation\b.*"
        ],
        "not match": [],
        "dtype_a": "count",
        "default_name": "Продление притока"
    },
    "pro_long_number": {
        "match": [
            r"number_of_prolongations",
            r".*\bnumber\b.*\bof\b.*\bprolongations\b.*"
        ],
        "not match": [],
        "dtype_a": "count",
        "default_name": "Количество продлений"
    },
    "inf_repay": {
        "match": [
            r"inflow_repayment",
            r".*\binflow\b.*\brepayment\b.*"
        ],
        "not match": [],
        "dtype_a": "count",
        "default_name": "Погашение притока"
    },
    "last_pay_in_fl_amount": {
        "match": [
            r"last_payment_inflow_amount",
            r".*\blast\b.*\bpayment\b.*\binflow\b.*\bamount\b.*"
        ],
        "not match": [],
        "dtype_a": "date",
        "default_name": "Дата последнего поступления оплаты"
    },
# перечисленние платежей в неизвенстном ыормате через ;
# возможно пригодится
    "pay_history_raw": {
        "match": [
            r"payment_history",
            r".*\bpayment\b.*\bhistory\b.*"
        ],
        "not match": [],
        "dtype_a": "char",
        "default_name": "История платежей"
    },
    "app_count": {
        "match": [
            r"application_counter",
            r".*\bapplication\b.*\bcounter\b.*"
        ],
        "not match": [],
        "dtype_a": "count",
        "default_name": "Счетчик обращений"
    },
    "loan_count": {
        "match": [
            r"loan_counter",
            r".*\bloan\b.*\bcounter\b.*"
        ],
        "not match": [],
        "dtype_a": "count",
        "default_name": "Счетчик кредитов"
    },
    "out_balance_tl_today": {
        "match": [
            r"oustanding_balance_till_today",
            r".*\boutstanding\b.*\bbalance\b.*\btill\b.*\btoday\b.*"
        ],
        "not match": [],
        "dtype_a": "sum",
        "default_name": "Непогашенный остаток на текущий день"
    },
    "b_debtor_phone_home": {
        "match": [
            r"телефон да/нет",
        ],
        "not match": [],
        "dtype_a": "bool",
        "default_name": "признак наличия домашнего телефона"
    },

    "accrued_interest": {
        "match": [
            r"voucher_comission/  проценты за пользование займом до просрочки",
            r"VOUCHER_COMMISSION".lower(),
            r"boni overdue / проценты за просрочку"
        ],
        "not match": [],
        "dtype_a": "sum",
        "default_name": "Проценты начисленные"
    },
    "debtor_citizenship": {
        "match": [
            "гражданство"
        ],
        "not match": [],
        "dtype_a": "factor",
        "default_name": "Гражданство"
    },
    "bank_account": {
        "match": [
            r".*\bсчёт\b.*",
            r"\bном.*\bсчет.*",
            r"\bлиц.*\bсчет.*"
        ],
        "not match": [
            r".*\bв счёт\b.*",
            r".*\bза счёт\b.*",
        ],
        "dtype_a": "char",
        "default_name": "Номер счёта должника"
    }

}

dtype_a_regex = {
    "date": [
        r"\s.*дата\s.*"
    ],
    "sum": [
        r".*\s*сумма\s.*",
        r"од",
        r"основной долг\s.*",
        r"проценты\s.*",
        r"пени\s.*",
        r"неустойк.*",
        r"штраф.*"
    ],
    "count": [
        r"количество\s.*",
        r"dpd",
        r".*число дней.*",
        r"срок\s.*",
        r"число\s.*",
    ],
    "other_numeric": [
        r".* ставка .*",
        r"размер\s.*"
    ],
    "boolean": [
        r".\bфлаг\b.*",
        r".\bпризнак\b.*",
        r".\bявляется\b.*",

    ]
}

dtype_a_dtype = {
    "id": ["object", "int64", "int", "int8", "int16"],
    "factor": ["object", "int64", "int"],
    "date": ["datetime64[ns]", "datetime64"],
    "sum": ["int64", "int", "float", "float64", "int8", "int16"],
    "count": ["int64", "int", "float", "float64", "int8", "int16"],
    "other_numeric": ["int64", "int", "float", "float64", "int8", "int16"],
    "bool": ["bool"],
    "char": ["object"]
}

gen_bucket_vars = {
    "dpd_buckets": {
        "base": "dpd",
        "dtype_a": "factor",
        "new_orig_key": "Бакеты просрочки",
        "new_key_index": "dpd_buckets_index",
        "bins": [
            np.NINF, -1, 1, 90, 180, 360, 540, 720, 1080, 1440, 1800,
            2160, 2520, 2880, 3240, np.Inf
        ],
        "labels": [
            'NA', '0', '0-90', '90-180', '180-360', '360-540',
            '540-720', '720-1080', '1080-1440', '1440-1800',
            '1800-2160', '2160-2520', '2520-2880', '2880-3240',
            '3240+'
        ]
    },
    "claim_sum_buckets": {
        "base": "claim_sum",
        "dtype_a": "factor",
        "new_orig_key": "Бакеты суммы исковых требований",
        "new_key_index": "debt_sum_buckets_index",
        "bins": [
            np.NINF, -1, 1, 7000, 14000, 30000, 60000, 120000,
            240000, 500000, 1000000, 2000000, np.Inf
        ],
        "labels": [
            'NA', '0', '0-7', '7-14', '14-30', '30-60',
            '60-120', '120-240', '240-500', '500-1000',
            '1000-2000', '2000+'
        ]

    },
    "principal_buckets": {
        "base": "debt_principal",
        "dtype_a": "factor",
        "new_orig_key": "Бакеты суммы ОД",
        "new_key_index": "principal_buckets_index",
        "bins": [
            np.NINF, -1, 1, 7000, 14000, 30000, 60000, 120000,
            240000, 500000, 1000000, 2000000, np.Inf
        ],
        "labels": [
            'NA', '0', '0-7', '7-14', '14-30', '30-60',
            '60-120', '120-240', '240-500', '500-1000',
            '1000-2000', '2000+'
        ]
    }
}

gen_boolean_vars = {
    "legal_approp": {
        "positives": [
            "b_approp",
            "b_has_docs",
            "b_has_passport",
        ],
        "negatives": [
            "b_bankrupcy",
            "b_act_of_impossibility",
            "b_death",
            "b_exec_ended",
            "b_fraud",
        ],
        "default_name": "Пригоден к судебному взысканию"
    },
    "pre_legal_approp": {
        "positives": [
            "b_approp"
        ],
        "negatives": [
            "b_bankrupcy",
            "b_act_of_impossibility",
            "b_death",
            "b_exec_ended",
            "b_contacts_refusal",
            "b_fraud"
        ],
        "default_name": "Пригоден к досудебной работе"
    },

}

required_fields = [
    'payments_holdhistory_sum', 'debt_principal', 'debt_interest', 'dpd'
]

booleans_list = {
    True: [
        'да', 1, '1', r'\+', 'yes', 'y', 'истина'
    ],
    False: [
        'нет', 0, '0', r'\-', '', 'no', 'n', 'ложь'
    ]
}

bad_symbols = {
    'date': [
        r'г\.', r'г'
    ],
    'sum': [
        r'рублей', r'руб\.', r'р\.', r'р', r'\s'
    ],
    'other_numeric': [
        r'дней', r'день', r'%/дн.', r'% в день', r'дня', r'дни', r'дн\.', r'дн', r'\s'
    ],
    'count': [
        r'дней', r'день', r'%/дн\.', r'% в день', r'дня', r'дни', r'дн\.', r'дн', r'\s'
    ]
}
