import pandas as pd
import operator
from .data_preprocess import get_names
from .colnames_recognition import *
from collections import Counter


def title_gen(df, df_cols):
    cases_count = len(df[get_names('id', df_cols)[0]].unique())
    debt_sum = df[get_names('claim_sum', df_cols)[0]].sum()
    avg_dpd = df[get_names('dpd', df_cols)[0]].mean()

    info = {
        "Показатель": ["Число кейсов"],
        "ед. изм.": ["шт"],
        "Значение": ["{:.0f}".format(cases_count)],
        "Доля": [""]
    }

    info['Показатель'].append("Средние исковые требования")
    info['ед. изм.'].append("руб.")
    info['Значение'].append("{:,.0f}".format(debt_sum / cases_count).replace(',', ' '))
    info['Доля'].append("")

    info['Показатель'].append("Средний срок просрочки")
    info['ед. изм.'].append("дней")
    info['Значение'].append("{:,.0f}".format(avg_dpd).replace(',', ' '))
    info['Доля'].append("")

    sums_list = [
        'debt_principal', 'debt_interest',
        'court_fee', 'payments_holdhistory_sum'
    ]

    if 'debt_sum_original' not in df_cols['known_colname'].tolist():
        name_sum = ['claim_sum']
    else:
        name_sum = ['debt_sum_original', 'claim_sum']

    for name in name_sum + sums_list:
        for k in get_names(name, df_cols):
            info['Показатель'].append(k)
            info['ед. изм.'].append("тыс. руб.")
            info['Значение'].append("{:,.0f}".format(df[k].sum() / 1000).replace(',', ' '))
            info['Доля'].append("{:.1%}".format(df[k].sum() / df[get_names('claim_sum', df_cols)[0]].sum()))

    # узнаю, не повторяет ли сумма исковых другую сумму:
    df_result = pd.DataFrame(info)
    debt_sum = df_result.loc[df_result['Показатель'] == get_names('claim_sum', df_cols)[0], 'Значение']
    df_result_sum_duplicates = df_result['Значение'].value_counts()
    # если повторяет, убираю ее
    if df_result_sum_duplicates[debt_sum].max() > 1:
        df_result = df_result.loc[df_result['Показатель'] != get_names('claim_sum', df_cols)[0]]

    return df_result


def unaprop_gen(df, df_cols):
    b_list = ['b_approp', 'b_has_docs', 'b_bankrupcy', 'b_act_of_impossibility', 'b_death', 'b_exec_ended', "b_contacts_refusal", "b_has_offer", "b_has_passport"]
    cases_count = len(df[get_names('id', df_cols)[0]].unique())

    info = {
        "Показатель": ["Число кейсов"],
        "ед. изм.": ["шт"],
        "Значение": ["{:.0f}".format(cases_count)],
        "Доля": [""]
    }

    for known_colname in b_list:
        if known_colname in df_cols['known_colname'].tolist():
            value = df[get_names(known_colname, df_cols)[0]].sum()
            if isinstance(value, pd.Series):
                value = value.max()
            info['Показатель'].append(get_names(known_colname, df_cols)[0])
            info['ед. изм.'].append("шт")
            info['Значение'].append("{:,.0f}".format(value).replace(',', ' '))
            info['Доля'].append("{:.1%}".format(value / cases_count))

    return pd.DataFrame(info)


def data_single_check(df, df_cols, rule_code, rule_name, cols_check, op_str, df_index):
    """
    :param df: исходный df
    :param df_cols: исходный df_cols
    :param rule_code: краткое название правила, любая строка, будет заголовком нового столбца df
    :param rule_name: развернутая название правила, любая строка
    :param cols_check: столбцы, к которым нужно применить правило (2 шт), list
    :param op_str: тип операции, строка (см словарь ops)
    :param df_index: индекс для df_result с обзором проверок
    :return: (df, df_cols, df_result)
    df - исходная df + новый столбец
    df_cols - исходная df_cols + новый столбец
    df_result - обзор примененного правила (1 строка)
    """
    ops = {
        '+': operator.add,
        '-': operator.sub,
        '*': operator.mul,
        '/': operator.truediv,
        '>': operator.gt,
        '>=': operator.ge,
        '<': operator.lt,
        '<=': operator.le,
        '==': operator.eq,
        '!=': operator.ne
    }
    op = ops[op_str]
    if all(col in df_cols['known_colname'].tolist() for col in cols_check):
        df[rule_code] = \
            op(df[get_names(cols_check[0], df_cols)[0]], df[get_names(cols_check[1], df_cols)[0]])

        df_result = pd.DataFrame({
            'rule_code': rule_code,
            'rule_name': rule_name,
            'rule_checks': df[rule_code].all(),
            'rule_violating_cnts': len(df.index) - df[rule_code].sum(),
            'rule_na_cnts': df[get_names(cols_check, df_cols)].isna().any(axis=1).sum()
        }, index=[df_index])

        df_cols = df_cols.append(
            pd.DataFrame({
                'colname': rule_code,
                'known_colname': rule_code,
                'dtype': df[rule_code].dtype,
                'dtype_a': 'bool',
                'dtype_check': 'OK',
                'unique_cnt': len(df.loc[~df[rule_code].isna()][rule_code].unique()),
                'empty_cnt': len(pd.isna(df[rule_code]))
            },
                index=[len(df_cols.index)],
            ), sort=False
        )

    else:
        val = ''
        for col in cols_check:
            if col not in df_cols['known_colname'].tolist():
                val += ' {} not in df '.format(col)
        df_result = pd.DataFrame({
            'rule_code': rule_code,
            'rule_name': rule_name,
            'rule_checks': val,
            'rule_violating_cnts': val,
            'rule_na_cnts': val
        }, index=[df_index])

    return df, df_cols, df_result


def data_check(df, df_cols):
    df_checks = pd.DataFrame()

    df, df_cols, df_check_result = data_single_check(
        df=df,
        df_cols=df_cols,
        rule_code='credit_date_rule_1',
        rule_name='Дата выдачи договора всегда раньше даты окончания',
        cols_check=['credit_date_created', 'credit_date_end'],
        op_str='<',
        df_index=0
    )
    df_checks = df_checks.append(df_check_result, sort=False)

    df, df_cols, df_check_result = data_single_check(
        df=df,
        df_cols=df_cols,
        rule_code='birthday_rule_1',
        rule_name='Дата рождения всегда раньше даты выдачи договора',
        cols_check=['birthday', 'credit_date_created'],
        op_str='<',
        df_index=0
    )
    df_checks = df_checks.append(df_check_result, sort=False)

    df, df_cols, df_check_result = data_single_check(
        df=df,
        df_cols=df_cols,
        rule_code='dpd_rule_1',
        rule_name='Дата выдачи договора всегда раньше даты выхода на просрочку',
        cols_check=['credit_date_created', 'overdue_date'],
        op_str='<',
        df_index=0
    )
    df_checks = df_checks.append(df_check_result, sort=False)

    return df, df_cols, df_checks


def intersection(lst1, lst2):
    lst3 = [value for value in lst1 if value in lst2]
    return lst3


def count_series_filterted_by_list(input_series, input_list):
    return pd.Series(Counter(intersection(input_series, input_list)))
