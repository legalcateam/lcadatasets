import pandas as pd
import numpy as np
import os
import re
import regex
import hashlib
import random
import string
import datetime
import sys
from dateutil.parser import parse as date_parse

from .colnames_recognition import \
    colnames_instructions, dtype_a_regex, dtype_a_dtype, gen_bucket_vars, required_fields, booleans_list, bad_symbols, \
    gen_boolean_vars
from .mfo_interest_calc import calculate_claim_sum_mfo

pd.options.mode.chained_assignment = None


def calculate_age(birthdate):
    today = datetime.date.today()
    age = today.year - birthdate.year - (
            (today.month, today.day) < (birthdate.month, birthdate.day)
    )
    return age


def substrings_strong(str_input, seps, min_len=1):
    if isinstance(seps, str):
        seps = [seps]

    default_sep = ';'

    for sep in seps:
        str_input = str_input.replace(sep, default_sep)

    split_str = str_input.split(default_sep)

    result = []
    for s in split_str:
        if len(s) >= min_len:
            result.append(s)

    return result


def get_dtype_a(values):
    """
    :param values: series
    :return: list of possible dtype_a of series
    """

    # 1) извлекаем список соответствий dtype_a и dtype
    dtypes = {
        "dtype": [],
        "dtype_a": []
    }

    for k, lst in dtype_a_dtype.items():
        for v in lst:
            dtypes['dtype'].append(v)
            dtypes['dtype_a'].append(k)

    df_dtypes = pd.DataFrame(dtypes)

    # 2) извлекаем список однозначных соответствий dtype и dtype_exact
    df_dtypes_cnt = df_dtypes['dtype'].value_counts()
    df_dtypes_unique = df_dtypes_cnt[df_dtypes_cnt == 1].index
    df_dtypes_exact = df_dtypes.loc[df_dtypes['dtype'].isin(df_dtypes_unique)]

    # 3) смотрим dtype присланной series
    dtype_original = values.dtype

    # 4) Смотрим, можем ли мы однозначно сопоставить dtype и dtype_a
    if dtype_original in df_dtypes_exact['dtype'].tolist():
        dtype_a = df_dtypes_exact.loc[df_dtypes_exact['dtype'] == dtype_original, 'dtype_a'].tolist()

    elif len(values.unique()) <= 2:
        dtype_a = ['bool']

    elif dtype_original == 'object':

        bad_symbols_list = []

        for k, val in bad_symbols.items():
            bad_symbols_list = bad_symbols_list + val

        bad_symbols_list = set(bad_symbols_list)
        values_cleanup = values.replace(bad_symbols_list, '', regex=True).astype(str)

        values_to_date = values_cleanup.apply(dateparser_with_na)
        values_to_numeric = pd.to_numeric(
            values_cleanup.str.replace(',', '.'), errors='coerce')

        dtype_a = []

        if values_to_date.isna().sum() / len(values) < 0.98:
            if len(values_to_date.loc[~values_to_date.isna()].unique()) > 2:
                dtype_a.append('date')
        if values_to_numeric.isna().sum() / len(values) < 0.98:
            if len(values_to_numeric.loc[~values_to_numeric.isna()].unique()) > 2:
                dtype_a.extend(['id', 'factor', 'sum', 'other_numeric'])
        if len(dtype_a) == 0:
            dtype_a = ['id', 'factor', 'char']

    else:
        dtype_a = df_dtypes.loc[df_dtypes['dtype'] == dtype_original, 'dtype_a'].tolist()

    return dtype_a


def recognise_colname(colname, values):
    """
    :param colname: original name of the column to b e recognised
    :param values: series with values of colname to be recognised
    :return: recognised standard name of the column
    """
    rec_names = {
        "key": [],
        "dtype_a": []
    }

    for key in colnames_instructions:
        if match_not_match(
                string=colname,
                match_list=colnames_instructions[key]['match'],
                not_match_list=colnames_instructions[key]['not match']
        ):
            rec_names['key'].append(key)
            rec_names['dtype_a'].append(colnames_instructions[key]['dtype_a'])

    if len(rec_names['key']) == 0:
        return 'unknown', 'unknown'
    elif len(rec_names['key']) == 1:
        return rec_names['key'][0], rec_names['dtype_a'][0]
    else:
        df_rec_names = pd.DataFrame(rec_names)
        dtype_a = get_dtype_a(values)
        df_result = df_rec_names.loc[df_rec_names['dtype_a'].isin(dtype_a)]
        if len(df_result.index) > 1:
            print(
                "##################################################################\n"
                "WARNING: column {} fits {}. First value returned\n"
                "##################################################################".format(
                    colname, df_result['key'].tolist()
                )
            )

        if len(df_result.index) == 0:
            print(
                "##################################################################\n"
                "WARNING: column {} is of dtype {}, found colnames {} are of dtypes {}. First value returned\n"
                "##################################################################".format(
                    colname, dtype_a, rec_names['key'], rec_names['dtype_a']
                )
            )
            return rec_names['key'][0], rec_names['dtype_a'][0]

        return df_result.loc[df_result.index[0], 'key'], df_result.loc[df_result.index[0], 'dtype_a']


def hash_series(s):
    """
    source: https://medium.com/luckspark/hashing-pandas-dataframe-column-with-nonce-763a8c23a833
    :param s: series should be hashed. Only strings without na's accepted.
    :return: series with hashes of original data
    """
    # Get a unique list of the clear text, as a List
    tmplist = list(set(s))

    # Add some random characters before and after the team name.
    # Structured them in a Dictionary
    # Example -- Liverpool -> aaaaaaaLiverpoolbbbbbbbb
    mapping1 = {i: (''.join(random.choice(string.hexdigits) for i in range(12))) + i + (
        ''.join(random.choice(string.hexdigits) for i in range(12))) for i in tmplist}

    # Create a new series containing clear_text_Nonce
    s_new = [mapping1[i] for i in s]

    # Hash the clear_text+Nonce string
    s_hashed = [hashlib.sha1(str.encode(str(i))).hexdigest() for i in s_new]

    return s_hashed


def check_dtype_a(colname, dtype='unknown', known_dtype_a='unknown'):
    if known_dtype_a != 'unknown':
        return known_dtype_a
    else:
        for key in dtype_a_regex:
            for r in dtype_a_regex[key]:
                if re.fullmatch(r, colname.lower()):
                    return key
        if dtype == 'object':
            return 'factor'
        elif dtype == 'datetime64[ns]':
            return 'date'
        elif dtype in ['int64', 'int', 'float', 'float64']:
            return 'other_numeric'
        else:
            return 'unknown'


def check_error(colname, dtype_a, dtype):
    if dtype_a == 'unknown':
        return 'unknown'
    else:
        dtypes_list = dtype_a_dtype.get(dtype_a)
        if dtypes_list is None:
            return 'TYPE ERROR'
        else:
            if dtype in dtypes_list:
                return 'OK'
            else:
                return 'ERROR'


def put_space(s):
    # regex [[:upper:]][^[:upper:]]* means any string starting
    # with capital character followed by many
    # lowercase letters
    words = regex.findall(r'[[:upper:]][^[:upper:]]*', s)

    if len(words) == 0:
        return s

    if not s.startswith(words[0]):
        first_word = regex.sub(r'[[:upper:]][^[:upper:]]*', '', s)
        words.insert(0, first_word)

    return ' '.join(words)


def match_not_match(string, match_list, not_match_list):
    match = False
    not_match = False

    for r in match_list:
        if re.match(r, string.lower()):
            match = True
        elif re.match(r, string.lower().replace("_", " ")):
            match = True
        elif re.match(r, put_space(string).lower()):
            match = True

    if match:
        for r in not_match_list:
            if re.match(r, string.lower()):
                not_match = True
            elif re.match(r, string.lower().replace("_", " ")):
                not_match = True
            elif re.match(r, put_space(string).lower()):
                not_match = True

    if match and not not_match:
        return True
    else:
        return False


def dateparser_with_na(string_date):
    if isinstance(string_date, np.datetime64):
        return string_date
    if isinstance(string_date, datetime.datetime):
        result = pd.to_datetime(string_date)
        return result
    try:
        if re.match('\d\d\d\d\D\d\d\D\d\d.*', string_date):
            result = pd.to_datetime(
                date_parse(string_date, dayfirst=False, yearfirst=True)
            )
        else:
            result = pd.to_datetime(
                date_parse(string_date, dayfirst=True, yearfirst=False)
            )
        return result
    except (ValueError, TypeError,
            pd._libs.tslibs.np_datetime.OutOfBoundsDatetime):
        return np.datetime64("NaT")
    except Exception as e:
        raise Exception(string_date, e)


def create_buckets(new_var_name, df_cols, df):
    d = gen_bucket_vars[new_var_name]
    key = d['base']
    if len(df_cols.loc[df_cols['known_colname'] == key].index) == 0:
        raise Exception("{} не найден в таблице".format(key))
    orig_key = df_cols.loc[df_cols['known_colname'] == key, 'colname'].iloc[0]
    new_key = new_var_name
    new_orig_key = d['new_orig_key']
    new_key_index = d['new_key_index']
    dtype_a = d['dtype_a']
    bins = d['bins']
    labels = d['labels']

    labels_index = dict(zip(labels, range(len(labels))))

    df[new_orig_key] = pd.cut(
        df[orig_key], bins=bins, labels=labels, include_lowest=True
    ).fillna('NA')

    df[new_key_index] = df[new_orig_key].map(labels_index)

    df_cols = df_cols.append(
        pd.DataFrame({
            'colname': new_orig_key,
            'known_colname': new_key,
            'dtype': df[new_orig_key].dtype,
            'dtype_a': dtype_a,
            'dtype_check': 'OK',
            'unique_cnt': len(df[new_orig_key].unique()),
            'empty_cnt': len(df.loc[df[new_orig_key] == 'NA'])
        },
            index=[len(df_cols.index)],
        ), sort=False
    )
    return df_cols


def newvar_sum(df, df_cols, source_keys, new_orig_key, new_key, dtype_a):
    source_orig_keys = df_cols.loc[
        df_cols['known_colname'].isin(source_keys), 'colname'
    ].tolist()

    df[new_orig_key] = df[source_orig_keys].sum(axis='columns')

    df_cols = df_cols.append(
        pd.DataFrame({
            'colname': new_orig_key,
            'known_colname': new_key,
            'dtype': df[new_orig_key].dtype,
            'dtype_a': dtype_a,
            'dtype_check': 'OK',
            'unique_cnt': len(df[new_orig_key].unique()),
            'empty_cnt': len(pd.isna(df[new_orig_key]))
        },
            index=[len(df_cols.index)]
        ), sort=False
    )
    return df, df_cols


def get_names(known_name, df_cols):
    """
    Возвращает все имена полей в df по заданным known_colname в df_cols
    :param known_name: list or str
    :param df_cols: вся таблица df_cols
    :return: list, перечень имен
    """
    if not isinstance(known_name, list):
        known_name = [known_name]
    return df_cols.loc[df_cols['known_colname'].isin(known_name), 'colname'].tolist()


def check_df_cols(df, df_cols):
    for col in df_cols['colname']:
        known_colname = df_cols.loc[df_cols['colname'] == col, 'known_colname']
        colname_params = colnames_instructions.get(known_colname.tolist()[0])

        col_dtype = df[col].dtype
        dtype_a = df_cols.loc[df_cols['colname'] == col, 'dtype_a'].iloc[0]
        df_cols.loc[df_cols['colname'] == col, 'dtype'] = col_dtype
        df_cols.loc[df_cols['colname'] == col, 'dtype_check'] = check_error(col, dtype_a, col_dtype)
        df_cols.loc[df_cols['colname'] == col, 'unique_cnt'] = len(df.loc[~df[col].isna()][col].unique())
        df_cols.loc[df_cols['colname'] == col, 'empty_cnt'] = df[col].isna().sum()

        if colname_params is not None:
            df_cols.loc[df_cols['colname'] == col, 'default_name'] = colname_params['default_name']
        else:
            df_cols.loc[df_cols['colname'] == col, 'default_name'] = None

    return df_cols


def sum_from_duplicate_colnames(df, df_cols):
    for colname, new_orig_key in {
        'debt_principal': 'Сумма ОД',
        'debt_interest': 'Сумма процентов',
        'debt_penalties': 'Сумма штрафов, пеней, неустоек'
    }.items():
        if len(df_cols.loc[df_cols['known_colname'] == colname].index) > 1:
            df_cols.loc[df_cols['known_colname'] == colname, 'known_colname'] = '{}_old'.format(colname)
            df, df_cols = newvar_sum(
                df, df_cols,
                source_keys=['{}_old'.format(colname)],
                new_orig_key=new_orig_key,
                new_key=colname,
                dtype_a='sum'
            )
    return df, df_cols


def cleanup_strings(df, df_cols):
    for k, bad_symbols_list in bad_symbols.items():
        for colname in df_cols.loc[df_cols['dtype_a'] == k, 'colname']:
            if df[colname].dtype == 'object':
                df[colname] = df[colname].replace(bad_symbols_list, '', regex=True).astype(str)

    return df, df_cols


def get_sex(s):
    men = [
        r"\bмуж.*",
        r"\bm.*",
        r"\bм.*",
        r"\bmale\b.*"
    ]
    women = [
        r"\bжен.*",
        r"\bw.*",
        r"\bf.*",
        r"\bж.*",
        r"\bfemale\b.*"
    ]

    if len(women) > len(men):
        length = len(women)
    else:
        length = len(men)

    if not isinstance(s, str):
        return s

    for i in range(length):
        if i < len(women):
            if re.match(women[i], s.lower()):
                return 0
        if i < len(men):
            if re.match(men[i], s.lower()):
                return 1

    return None


def read_table(*args):
    """
    Функция для чтения xls/xlsx и csv файлов. Нужна для облегчения работы с файлами различного формата.
    Function for reading xls/xlsx and csv files. Needed if you are working with different files from multiple sources
    :param work_dir: directory, where file is located (string)
    :param raw_xl: filename
    :return: pandas DataFrame
    """
    file_extension = os.path.splitext(args[-1])[1]

    if file_extension == '.csv':
        df = pd.read_csv(
            os.path.join(*args),
            dtype=str,
            sep=';',
            encoding='cp1251'
        )
    else:
        df = pd.read_excel(
            os.path.join(*args),
            thousands=' '
        )

    return df


def val_to_bool(s):
    if isinstance(s, bool):
        return s

    if isinstance(s, str):
        s = re.sub("\s*", "", s)

    for key, r_list in booleans_list.items():
        for r in r_list:
            if s == r:
                return key
            if re.fullmatch(str(r), str(s).lower()):
                return key

    return s


def bool_gen(s):
    if not isinstance(s, pd.Series):
        raise Exception('bool_gen accepts only series as argument')
    if s.dtype == 'bool':
        return s

    result = s.apply(val_to_bool)

    if len(result.unique()) > 2:
        result.loc[result.isna()] = False
        result = result.apply(
            lambda x: x if isinstance(x, bool) else True
        )

    return result


def create_boolean_vars(df, df_cols):
    b_list_exists = df_cols.loc[df_cols['dtype_a'] == 'bool', 'known_colname'].tolist()

    if len(b_list_exists) != 0:
        for bool_to_gen_name, bool_to_gen_dict in gen_boolean_vars.items():
            b_list_positives = bool_to_gen_dict['positives']
            b_list_negatives = bool_to_gen_dict['negatives']
            new_colname = bool_to_gen_dict['default_name']

            b_list_pos_exist = []
            b_list_neg_exist = []

            for el in b_list_exists:
                if el in b_list_positives:
                    b_list_pos_exist.append(el)
                if el in b_list_negatives:
                    b_list_neg_exist.append(el)

            df_approp = df[get_names(b_list_pos_exist + b_list_neg_exist, df_cols)]

            if len(b_list_neg_exist) >= 1:
                df_approp[get_names(b_list_neg_exist, df_cols)] = ~df_approp[get_names(b_list_neg_exist, df_cols)]

            df[new_colname] = df_approp.all(axis=1)

            df_cols = df_cols.append(
                pd.DataFrame({
                    'colname': bool_to_gen_dict['default_name'],
                    'known_colname': bool_to_gen_name,
                    'dtype': df[new_colname].dtype,
                    'dtype_a': 'bool',
                    'dtype_check': 'OK',
                    'unique_cnt': len(df.loc[~df[new_colname].isna()][new_colname].unique()),
                    'empty_cnt': len(pd.isna(df[new_colname])),
                    'default_name': bool_to_gen_name
                },
                    index=[len(df_cols.index)],
                ), sort=False
            )

    return df, df_cols


def debtor_id_gen(df, df_cols):
    if 'debtor_id' not in df_cols['known_colname'].tolist():
        if 'debtor_fio' in df_cols['known_colname'].tolist() and 'birthday' in df_cols['known_colname'].tolist():
            df['debtor_id'] = hash_series(
                df[get_names('debtor_fio', df_cols)[0]].astype(str) +
                df[get_names('birthday', df_cols)[0]].dt.strftime('%Y%m%d')
            )
            df_cols = df_cols.append(
                pd.DataFrame({
                    'colname': 'debtor_id',
                    'known_colname': 'debtor_id',
                    'dtype': df['debtor_id'].dtype,
                    'dtype_a': 'id',
                    'dtype_check': 'OK',
                    'unique_cnt': len(df.loc[~df['debtor_id'].isna()]['debtor_id'].unique()),
                    'empty_cnt': len(pd.isna(df['debtor_id']))
                },
                    index=[len(df_cols.index)],
                ), sort=False
            )
    return df, df_cols


def recognition(df):
    # df = read_table(work_dir, raw_xl)
    df_cols = pd.DataFrame({"colname": df.columns})

    # Собираю информацию о типах данных
    for col in df_cols['colname']:
        col_dtype = df[col].dtype
        known_colname, known_dtype_a = recognise_colname(col, df[col])
        colname_params = colnames_instructions.get(known_colname)
        df_cols.loc[df_cols['colname'] == col, 'known_colname'] = known_colname
        df_cols.loc[df_cols['colname'] == col, 'dtype'] = col_dtype
        dtype_a = check_dtype_a(col, col_dtype, known_dtype_a)
        df_cols.loc[df_cols['colname'] == col, 'dtype_a'] = dtype_a
        df_cols.loc[df_cols['colname'] == col, 'dtype_check'] = check_error(col, dtype_a, col_dtype)
        df_cols.loc[df_cols['colname'] == col, 'unique_cnt'] = len(df[col].unique())
        df_cols.loc[df_cols['colname'] == col, 'empty_cnt'] = df[col].isna().sum()
        if colname_params is not None:
            default_name = colname_params['default_name']
        else:
            default_name = None
        df_cols.loc[df_cols['colname'] == col, 'default_name'] = default_name

    # убираем мусор из числовых данных
    df, df_cols = cleanup_strings(df, df_cols)

    return df_cols


def prep(df, df_cols):

    # пытаемся преобразовать булево в булево
    for known_colname in df_cols.loc[df_cols['dtype_a'] == 'bool', 'known_colname'].tolist():
        old_preservation_list = get_names(known_colname, df_cols)
        for i in range(len(old_preservation_list)):
            old_preservation_list[i] += '_old'
        df[old_preservation_list] = df[get_names(known_colname, df_cols)]
        df[get_names(known_colname, df_cols)] = df[get_names(known_colname, df_cols)].apply(bool_gen)

        for colname in get_names(known_colname, df_cols):
            if df[colname].dtype != 'bool':
                print(
                    "#######################################################################\n"
                    "WARNING: can't convert column {}: {} to boolean\n"
                    "#######################################################################\n".format(
                        known_colname,
                        colname
                    )
                )

    # Разделяем вид и тип кредита
    if len(df_cols.loc[df_cols['known_colname'] == 'credit_type'].index) > 1:
        min_unique_cnt = df_cols.loc[df_cols['known_colname'] == 'credit_type', 'unique_cnt'].min()
        min_unique_cnt_colname = df_cols.loc[
            (df_cols['known_colname'] == 'credit_type') &
            (df_cols['unique_cnt'] == min_unique_cnt),
            'colname'
        ]
        if isinstance(min_unique_cnt_colname, str):
            df_cols.loc[
                (df_cols['colname'] != min_unique_cnt_colname) &
                (df_cols['known_colname'] == 'credit_type'),
                'known_colname'
            ] = 'credit_product_name'
        else:
            df_cols.loc[
                (~df_cols['colname'].isin(min_unique_cnt_colname.tolist())) &
                (df_cols['known_colname'] == 'credit_type'),
                'known_colname'
            ] = 'credit_product_name'

    # пытаюсь исправить ошибки в чтении дат
    for colname in df_cols.loc[
        (df_cols['dtype_check'] == 'ERROR') &
        (df_cols['dtype_a'] == 'date'),
        'colname'
    ]:
        try:
            df[colname] = df[colname].apply(dateparser_with_na)
        except Exception as e:
            print(
                'Попытка исправить ошибку в чтении дат не удалась:',
                'столбец {} \n'.format(colname),
                'ошибка {}'.format(e)
            )

    # на всякий случай округляю даты
    for colname in df_cols.loc[
        df_cols['dtype_a'] == 'date',
        'colname'
    ]:
        tz = 'MST'
        df[colname] = pd.to_datetime(df[colname], utc=True)
        df[colname] = df[colname].dt.tz_convert(tz)
        df[colname] = df[colname].dt.tz_localize(None)
        df[colname] = df[colname].dt.floor('1D')

    # пытаюсь исправить ошибки в чтении числовых полей
    for colname in df_cols.loc[
        (df_cols['dtype_check'] == 'ERROR') &
        (df_cols['dtype_a'].isin(['sum', 'other_numeric'])),
        'colname'
    ]:
        try:
            df[colname] = pd.to_numeric(
                df[colname].str.replace(',', '.'), errors='coerce')
        except Exception as e:
            tb = sys.exc_info()[2]
            raise Exception(f"Error on column {colname}: {e.with_traceback(tb)}")

    for colname in df_cols.loc[
        (df_cols['dtype_check'] == 'ERROR') &
        (df_cols['dtype_a'] == 'count'),
        'colname'
    ]:
        df[colname] = pd.to_numeric(
            df[colname].str.replace(',', '.'), errors='coerce', downcast='integer')

    return df, df_cols


def data_handling(df, df_cols, LOG_LEVEL='NONE', req_na='leave', mfo=False):

    # Убираю строки, в которых отсутствуют данные обязательных полей
    # если было указано
    if req_na != 'leave':
        if req_na == 'drop':
            for key in required_fields:
                names = get_names(key, df_cols)
                for name in names:
                    df = df.loc[~df[name].isna()]

    # Проверяю на наличие ошибок после исправления
    df_cols = check_df_cols(df, df_cols)

    # Проверяю количество столбцов с типом debt_principal, debt_interest, debt_penalties и создаю столбцы с суммой,
    # если их несколько
    df, df_cols = sum_from_duplicate_colnames(df, df_cols)

    # Вывожу распознанные и нераспознанные столбцы
    ##################################################################################
    if LOG_LEVEL == 'DEBUG':
        print('Найденные поля:')
        for i in df_cols.loc[df_cols['known_colname'] != 'unknown'].index:
            print('{}: {}'.format(df_cols.loc[i, 'known_colname'], df_cols.loc[i, 'colname']))
        print('#######################################################')
        print('Не найденные поля:')
        for key in colnames_instructions:
            if len(df_cols.loc[df_cols['known_colname'] == key].index) == 0:
                if key in required_fields:
                    print('# {}: Обязательное поле! #'.format(key))
                else:
                    print(key)
        print('#######################################################')
        print('Неизвестные поля:')
        for i in df_cols.loc[df_cols['known_colname'] == 'unknown'].index:
            print('{}: {}'.format(df_cols.loc[i, 'known_colname'], df_cols.loc[i, 'colname']))
    ####################################################################################

    # создаю переменную "id", если её нет
    if 'id' not in df_cols['known_colname'].tolist() and 'credit_number' in df_cols['known_colname'].tolist():
        new_orig_key = 'id'
        base = 'credit_number'
        new_key = 'id'
        dtype_a = 'id'

        df[new_orig_key] = df[get_names(base, df_cols)[0]]

        df_cols = df_cols.append(
            pd.DataFrame({
                'colname': new_orig_key,
                'known_colname': new_key,
                'dtype': df[new_orig_key].dtype,
                'dtype_a': dtype_a,
                'dtype_check': 'OK',
                'unique_cnt': len(df.loc[~df[new_orig_key].isna()][new_orig_key].unique()),
                'empty_cnt': len(pd.isna(df[new_orig_key]))
            },
                index=[len(df_cols.index)],
            ), sort=False
        )
    elif 'id' not in df_cols['known_colname'].tolist() and 'credit_number' not in df_cols['known_colname'].tolist():
        new_orig_key = 'id'
        new_key = 'id'
        dtype_a = 'id'

        df[new_orig_key] = df.reset_index().index

        df_cols = df_cols.append(
            pd.DataFrame({
                'colname': new_orig_key,
                'known_colname': new_key,
                'dtype': df[new_orig_key].dtype,
                'dtype_a': dtype_a,
                'dtype_check': 'OK',
                'unique_cnt': len(df.loc[~df[new_orig_key].isna()][new_orig_key].unique()),
                'empty_cnt': len(pd.isna(df[new_orig_key]))
            },
                index=[len(df_cols.index)],
            ), sort=False
        )

    # Создаю переменную debtor_id
    if 'debtor_id' not in df_cols['known_colname'].tolist():
        df, df_cols = debtor_id_gen(df, df_cols)

    # создаю переменную "dpd", если её нет
    if 'dpd' not in df_cols['known_colname'].tolist() and 'credit_date_created' in df_cols['known_colname'].tolist():
        new_orig_key = 'dpd'
        base = 'overdue_date'
        new_key = 'dpd'
        dtype_a = 'count'

        if base not in df_cols['known_colname'].to_list():
            raise ValueError("Neither dpd nor {} not in df_cols".format(base))

        df[new_orig_key] = ((pd.Timestamp.now() - df[get_names(base, df_cols)[0]]) / pd.Timedelta('1 day')).round(0)

        df_cols = df_cols.append(
            pd.DataFrame({
                'colname': new_orig_key,
                'known_colname': new_key,
                'dtype': df[new_orig_key].dtype,
                'dtype_a': dtype_a,
                'dtype_check': 'OK',
                'unique_cnt': len(df.loc[~df[new_orig_key].isna()][new_orig_key].unique()),
                'empty_cnt': len(pd.isna(df[new_orig_key]))
            },
                index=[len(df_cols.index)],
            ), sort=False
        )

    # Создаю новые переменные
    df_cols = create_buckets('dpd_buckets', df_cols, df)
    df_cols = create_buckets('principal_buckets', df_cols, df)

    # создаю переменную "overdue_date", если её нет
    # if 'overdue_date' not in df_cols['known_colname'].tolist() and 'dpd' in df_cols['known_colname'].tolist():
    #     new_orig_key = 'overdue_date'
    #     base = 'dpd'
    #     new_key = 'overdue_date'
    #     dtype_a = 'count'
    #
    #     if base not in df_cols['known_colname'].to_list():
    #         raise ValueError("Neither overdue_date nor {} not in df_cols".format(base))
    #
    #     df[new_orig_key] = ((pd.Timestamp.now() - df[get_names(base, df_cols)[0]]) / pd.Timedelta('1 day')).round(0)
    #
    #     df_cols = df_cols.append(
    #         pd.DataFrame({
    #             'colname': new_orig_key,
    #             'known_colname': new_key,
    #             'dtype': df[new_orig_key].dtype,
    #             'dtype_a': dtype_a,
    #             'dtype_check': 'OK',
    #             'unique_cnt': len(df.loc[~df[new_orig_key].isna()][new_orig_key].unique()),
    #             'empty_cnt': len(pd.isna(df[new_orig_key]))
    #         },
    #             index=[len(df_cols.index)],
    #         ), sort=False
    #     )
    #
    # # Создаю новые переменные
    # df_cols = create_buckets('overdue_date', df_cols, df)

    # создаю переменную "Наличие платежей"
    if 'payments_holdhistory_sum' in df_cols['known_colname'].tolist():
        new_orig_key = "Наличие платежей"
        base = 'payments_holdhistory_sum'
        new_key = 'has_payments'
        dtype_a = "boolean"

        df[new_orig_key] = (
                df[get_names(base, df_cols)[0]] > 500
        ).fillna(False)

        df_cols = df_cols.append(
            pd.DataFrame({
                'colname': new_orig_key,
                'known_colname': new_key,
                'dtype': df[new_orig_key].dtype,
                'dtype_a': dtype_a,
                'dtype_check': 'OK',
                'unique_cnt': len(df.loc[~df[new_orig_key].isna()][new_orig_key].unique()),
                'empty_cnt': len(pd.isna(df[new_orig_key]))
            },
                index=[len(df_cols.index)],
            ), sort=False
        )

    # создаю переменную age, если её нет
    if 'age' not in df_cols['known_colname'].tolist() and 'birthday' in df_cols['known_colname'].tolist():
        new_orig_key = "Возраст должника"
        base = 'birthday'
        new_key = 'age'
        dtype_a = "count"

        df[new_orig_key] = df[get_names(base, df_cols)[0]].apply(
            lambda x: calculate_age(x)
        )

        df_cols = df_cols.append(
            pd.DataFrame({
                'colname': new_orig_key,
                'known_colname': new_key,
                'dtype': df[new_orig_key].dtype,
                'dtype_a': dtype_a,
                'dtype_check': 'OK',
                'unique_cnt': len(df.loc[~df[new_orig_key].isna()][new_orig_key].unique()),
                'empty_cnt': len(pd.isna(df[new_orig_key]))
            },
                index=[len(df_cols.index)],
            ), sort=False
        )

    # создаю переменную "sex_num"
    if 'sex' in df_cols['known_colname'].tolist():
        new_orig_key = "sex_num"
        base = 'sex'
        new_key = 'sex_num'
        dtype_a = "count"

        df[new_orig_key] = df[get_names(base, df_cols)[0]].apply(
            lambda x: get_sex(x)
        )

        # Для неопределенного пола временно ставлю 1, т.к. мужчины выходят на пенсию позже
        df.loc[~df[new_orig_key].isin([0, 1]), new_orig_key] = 1

        df_cols = df_cols.append(
            pd.DataFrame({
                'colname': new_orig_key,
                'known_colname': new_key,
                'dtype': df[new_orig_key].dtype,
                'dtype_a': dtype_a,
                'dtype_check': 'OK',
                'unique_cnt': len(df.loc[~df[new_orig_key].isna()][new_orig_key].unique()),
                'empty_cnt': len(pd.isna(df[new_orig_key]))
            },
                index=[len(df_cols.index)],
            ), sort=False
        )

    # создаю переменную "количество телефонов"
    if 'debtor_phones_cnt' not in df_cols['known_colname'].tolist():
        new_orig_key = "debtor_phones_cnt"
        new_key = 'debtor_phones_cnt'
        dtype_a = "count"

        df[new_orig_key] = [None] * len(df.index)
        phone_cols = [
            'debtor_phone_home_reg',
            'debtor_phone_home_fact',
            'debtor_phone_job',
            'debtor_phone_mobile',
            'debtor_phone_third_party'
        ]

        phone_separators = [',', ';', '|', '+', '{', '}', '[', ']']

        if df_cols['known_colname'].isin(phone_cols).any():
            phones_all = pd.Series([''] * len(df.index)).str.cat(
                df[get_names(phone_cols, df_cols)].astype(str), sep=";", na_rep=''
            )

            df[new_orig_key] = phones_all.apply(
                lambda x: len(substrings_strong(x, phone_separators, 9))
            )

        df_cols = df_cols.append(
            pd.DataFrame({
                'colname': new_orig_key,
                'known_colname': new_key,
                'dtype': df[new_orig_key].dtype,
                'dtype_a': dtype_a,
                'dtype_check': 'OK',
                'unique_cnt': len(df.loc[~df[new_orig_key].isna()][new_orig_key].unique()),
                'empty_cnt': len(pd.isna(df[new_orig_key])),
                'default_name': colnames_instructions[new_key]['default_name']
            },
                index=[len(df_cols.index)],
            ), sort=False
        )

    # создаю новые булевы переменные
    df, df_cols = create_boolean_vars(df, df_cols)

    # считаю исковые требования для МФО
    if mfo:
        rename_dict = {
            df_cols.loc[i, 'colname']: df_cols.loc[i, 'known_colname']
            for i in df_cols.loc[~df_cols['known_colname'].isna()].index
        }
        colnames = [
                'credit_date_created',
                'debt_principal',
                'debt_interest',
                'credit_rate',
                'credit_term_days',
                'credit_sum'
            ]

        # Проверка на то, что каждое значение known_colname соответствует строго одному значению colname

        for colname in colnames:
            if len(get_names([colname], df_cols)) > 1:
                raise ValueError(
                    '{} has several possible recognized values: {}'.format(colname, get_names([colname], df_cols))
                )

        df_work = df[
            get_names(colnames, df_cols)
        ].rename(columns=rename_dict)

        df[
            ['claim_sum', 'claim_interest', 'claim_calc_type']
        ] = calculate_claim_sum_mfo(df_work)
    else:
        df['claim_interest'] = df[get_names('debt_interest', df_cols)[0]]
        df['claim_sum'] = df[get_names('debt_interest', df_cols)[0]] + df[get_names('debt_principal', df_cols)[0]]
        df['claim_calc_type'] = 'interest_full_sum'
    for colname, dtype_a in {'claim_sum': 'sum', 'claim_interest': 'sum', 'claim_calc_type': 'factor'}.items():
        default_name = colnames_instructions[colname]['default_name']
        df = df.rename(columns={colname: default_name})
        df_cols = df_cols.append(
            pd.DataFrame({
                'colname': default_name,
                'known_colname': colname,
                'dtype': df[default_name].dtype,
                'dtype_a': dtype_a,
                'dtype_check': 'OK',
                'unique_cnt': len(df.loc[~df[default_name].isna()][default_name].unique()),
                'empty_cnt': len(pd.isna(df[default_name])),
                'default_name': default_name
            },
                index=[len(df_cols.index)],
            ), sort=False
        )

    df_cols = create_buckets('claim_sum_buckets', df_cols, df)

    # Снова проверяю данные в df
    df_cols = check_df_cols(df, df_cols)

    return df, df_cols
