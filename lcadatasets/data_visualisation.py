from .colnames_recognition import gen_bucket_vars
from .data_preprocess import get_names
import pandas as pd


# Стандартное импортирование plotly
from plotly.offline import iplot

# Использование cufflinks в офлайн-режиме
import cufflinks

cufflinks.go_offline()

# Настройка глобальной темы cufflinks
cufflinks.set_config_file(world_readable=False, theme='pearl', offline=True)


def show_buckets(new_var_name, df, xtitle='', facet_row=None, df_cols=None):
    d = gen_bucket_vars[new_var_name]
    new_key = new_var_name
    new_orig_key = d['new_orig_key']
    new_key_index = d['new_key_index']

    facet_row_orig = []
    if facet_row is not None and df_cols is not None:
        facet_row_orig = get_names(facet_row, df_cols)

    if len(facet_row_orig) != 0:
        df_plot_prep = df[[new_orig_key, new_key_index, facet_row_orig[0]]]

        figs = []
        for val in df_plot_prep[facet_row_orig[0]].unique():

            col = df_plot_prep.loc[
                df_plot_prep[facet_row_orig[0]] == val
                ].sort_values(new_key_index)[new_orig_key].reset_index(drop=True)

            if isinstance(val, str):
                name = val
            else:
                name = 'Лот ' + str(val)

            col.name = name

            figs.append(
                col.figure(kind='hist',
                           xTitle=xtitle,
                           yTitle='Число кейсов',
                           name=name
                           )
            )

        plot = cufflinks.subplots(figs, shape=(len(figs), 1))
        plot['layout'].update(height=300*len(figs), title=new_orig_key)
        iplot(plot)
    else:
        df[[new_orig_key, new_key_index]]. \
            sort_values(by=new_key_index)[new_orig_key].iplot(
            kind='hist', xTitle=xtitle, yTitle='Число кейсов',
            title=new_orig_key)


def show_buckets_overlay(new_var_names, title, xtitle, df):
    new_orig_keys = []
    new_key_indexes = []
    for k in new_var_names:
        new_orig_keys.append(gen_bucket_vars[k]['new_orig_key'])
        new_key_indexes.append(gen_bucket_vars[k]['new_key_index'])
    columns = new_orig_keys + new_key_indexes
    df[columns]. \
        sort_values(by=new_key_indexes)[new_orig_keys].iplot(
        kind='hist',
        xTitle=xtitle,
        yTitle='Число кейсов',
        title=title,
        barmode='overlay'
    )


def show_years(short_names, df, df_cols, facet_row=None):
    orig_names = []
    for short_name in short_names:
        orig_names.extend(get_names(short_name, df_cols))

    df_temp = df[orig_names].apply(lambda col: col.dt.year)

    if facet_row is not None:
        facet_row_original = get_names(facet_row, df_cols)
    else:
        facet_row_original = []

    if len(facet_row_original) == 0:
        for var in df_temp:
            df_temp[var].iplot(
                kind='hist',
                yTitle='Число кейсов',
                title=var,
            )

    else:
        facet = facet_row_original[0]
        vars = df_temp.columns
        df_temp = pd.concat((df_temp, df[facet]), axis=1)
        for var in vars:
            df_plot_prep = df_temp[[var, facet]]
            figs = []
            for lot in df_plot_prep[facet].unique():
                col = df_plot_prep.loc[
                    df_plot_prep[facet] == lot
                    ].reset_index(drop=True)[var]
                if isinstance(lot, str):
                    name = lot
                else:
                    name = 'Лот ' + str(lot)
                col.name = name
                figs.append(
                    col.figure(kind='hist',
                               yTitle='Число кейсов',
                               name=name
                               )
                )

            plot = cufflinks.subplots(figs, shape=(len(figs), 1))
            plot['layout'].update(height=300 * len(figs), title=var)
            iplot(plot)


def show_months(short_names, df, df_cols):
    orig_names = []
    for short_name in short_names:
        orig_names.extend(get_names(short_name, df_cols))

    df_temp = df[orig_names].apply(
        lambda col: (col.dt.year * 100 + col.dt.month)
    )

    for var in df_temp:
        df_temp[var].iplot(
            kind='hist',
            yTitle='Число кейсов',
            title=var,
        )


def show_factor_piechart(short_names, df, df_cols, facet_row=None):
    orig_names = []
    for short_name in short_names:
        orig_names.extend(get_names(short_name, df_cols))

    if facet_row is not None:
        facet_row_original = get_names(facet_row, df_cols)
    else:
        facet_row_original = []

    if len(facet_row_original) == 0:
        for var in orig_names:
            summary = pd.DataFrame(df[var].value_counts()).reset_index().fillna('NA')
            summary.iplot(
                kind='pie', labels='index', values=var, title=var,
                textinfo='label+value+percent', textposition='inside'
            )
    else:
        for var in orig_names:
            figs = []
            for lot in df[facet_row_original[0]].unique():

                if isinstance(lot, str):
                    name = lot
                else:
                    name = 'Лот ' + str(lot)

                summary = pd.DataFrame(
                    df.loc[df[facet_row_original[0]] == lot, var].value_counts()
                ).reset_index().fillna('NA')

                summary.iplot(
                    kind='pie', labels='index', values=var, title=' '.join([var, name]),
                    textinfo='label+value+percent', textposition='inside'
                )


def show_hist(short_names, df, df_cols, facet_row=None):
    orig_names = []
    for short_name in short_names:
        orig_names.extend(get_names(short_name, df_cols))

    facet_row_orig = get_names(facet_row, df_cols)

    if facet_row is not None and len(facet_row_orig) != 0:
        for var in orig_names:
            df[var].iplot(
                kind='hist',
                yTitle='Число кейсов',
                title=var,
                facet_row=facet_row_orig[0]
            )
    else:
        for var in orig_names:
            df[var].iplot(
                kind='hist',
                yTitle='Число кейсов',
                title=var,
            )
