"""
This module calculate maximum interest for PDL loans according to Russian laws
"""

import pandas as pd
from pandas.core.dtypes.common import is_datetime64_any_dtype
from pandas.api.types import is_numeric_dtype


def guess_rate_type(s):
    """
    Пытается угадать, какой тип процентной ставки в массиве arr: годовая или % в день.
    Основано на сопоставлении размера ставки с фиксированными значениями
    Предполагается, что во всем массиве все ставки одного типа
    :param s: series с процентными ставками
    :return: string, 'Y' or 'D'
    """
    if not isinstance(s, pd.Series):
        raise TypeError(
            'Parameter s should be a series, not {}'.format(type(s))
        )

    if not is_numeric_dtype(s):
        raise TypeError(
            'Parameter s should be numeric-type, not {}'.format(s.dtype)
        )

    # делаем copy, чтобы избежать изменения первоначального объекта
    s_work = s.copy(deep=True)
    # убираем значения, равные нулю или nan
    s_work = s_work.loc[(s_work != 0) & (~s_work.isna())]

    # считаем минимум и максимум
    s_min = s_work.min()
    s_max = s_work.max()

    if s_max <= 5 and 0.7 < s_min <= 3:
        return 'D'
    elif s_max >= 10 and s_min > 5:
        return 'Y'
    elif s_max < 1.3 and s_min < 0.7:
        return 'Y/100'
    if s_max <= 0.05 and s_min <= 0.03:
        return 'D/100'


def calculate_claim_sum_mfo(df):
    """
    Рассчитывает сумму исковых требований, которую мы можем взыскать по долгам МФО
    согласно законодательству РФ
    :param df: pandas DataFrame with columns
    credit_date_created, debt_principal, debt_interest, credit_rate
    credit_date_created: series
    debt_principal: dtype - float
    debt_interest: dtype - float
    credit_rate: dtype - float, function will guess percent type (per year / per day)
    credit_sum: dtype - float
    :return: pandas DataFrame with columns ['claim_sum', 'claim_interest', 'claim_type']
    """
    cbrates = pd.read_pickle('data/cbrates.pkl')
    columns_work = [
        'credit_date_created',
        'debt_principal',
        'debt_interest',
        'credit_rate',
        'credit_term_days',
        'credit_sum'
    ]
    df_work = df[columns_work].copy(deep=True)

    if not is_datetime64_any_dtype(df_work['credit_date_created']):
        raise TypeError(
            'credit_date_created column should be of datetime64 dtype'
        )

    for column in [
        'debt_principal',
        'debt_interest',
        'credit_rate',
        'credit_term_days',
        'credit_sum'
    ]:
        if not is_numeric_dtype(df_work[column]):
            raise TypeError(
                '{} column should be of numeric dtype'.format(column)
            )
    # заполняем пропуски в credit_sum, если они есть
    df_work.loc[
        (df_work['credit_sum'] == 0) | df_work['credit_sum'].isna(),
        'credit_sum'
    ] = df_work.loc[
        (df_work['credit_sum'] == 0) | df_work['credit_sum'].isna(),
        'debt_principal'
    ]

    # дата окончания договора
    df_work['credit_date_end'] = \
        df_work['credit_date_created'] + \
        (pd.Timedelta('1 day') * df_work['credit_term_days'])

    # ставка ЦБ для текущего долга
    df_work['cb_rate'] = \
        df_work['credit_date_created'].apply(
            lambda x:
            cbrates.iloc[
                cbrates['date_start'].searchsorted(x, side='left') - 1,
                2
            ]
        )

    # Приводим ставку к годовым процентам в формате 10% == 0.1
    if guess_rate_type(df_work['credit_rate']) == 'D':
        df_work['credit_rate'] = df_work['credit_rate'] * 365 / 100
    elif guess_rate_type(df_work['credit_rate']) == 'Y':
        df_work['credit_rate'] = df_work['credit_rate'] / 100
    elif guess_rate_type(df_work['credit_rate']) == 'D/100':
        df_work['credit_rate'] = df_work['credit_rate'] * 365

    # Нулевые и отсутствующие ставки заменяем на стандартные (1,5% в день)
    df_work.loc[
        (df_work['credit_rate'] == 0) | df_work['credit_rate'].isna(),
        'credit_rate'
    ] = 0.015 * 365

    """
    до 28.03.2016 г. (включительно)
    сумма % рассчитывается по средневзвешенной ставке ЦБ РФ (в процентах годовых), 
    действующей в соответствующие периоды

    (ограничиваем 4 ОД)
    """
    date_cutoff_bottom = pd.Timestamp('1990-01-01')
    date_cutoff_top = pd.Timestamp('2016-03-28')
    loc_series = (df_work['credit_date_created'] <= date_cutoff_top) & (
            df_work['credit_date_created'] >= date_cutoff_bottom)

    df_work['interest_before_debt_end'] = (
            df_work['credit_term_days']
            * (df_work['credit_rate'] / 365)
            * df_work['credit_sum']
    )

    df_work['interest_after_debt_end_by_cbrate'] = (
            (
                    (pd.Timestamp.now().floor('1D') - df_work['credit_date_end'])
                    / pd.Timedelta('1 day')
            )
            * (df_work['cb_rate'] / 365)
            * df_work['credit_sum']
    )

    df_work['max_interest_by_cbrate'] = (
            df_work['interest_before_debt_end']
            + df_work['interest_after_debt_end_by_cbrate']
    )
    df_work['4x_principal'] = df_work['credit_sum'] * 4

    df_work.loc[loc_series, 'claim_interest'] = df_work.loc[
        loc_series,
        ['max_interest_by_cbrate', '4x_principal', 'debt_interest']
    ].min(axis='columns')
    df_work.loc[loc_series, 'claim_calc_type'] = 'cb_rate'

    """
    с 29.03.2016 по 31.12.2016 г. (включительно)
    Сумма начисленных % не должна превышать четырехкратный размер суммы займа (4 ОД) 
    """
    date_cutoff_bottom = pd.Timestamp('2016-03-29')
    date_cutoff_top = pd.Timestamp('2016-12-31')
    loc_series = (df_work['credit_date_created'] <= date_cutoff_top) & (
            df_work['credit_date_created'] >= date_cutoff_bottom)

    df_work.loc[loc_series, 'claim_interest'] = df_work.loc[
        loc_series, ['4x_principal', 'debt_interest']
    ].min(axis='columns')
    df_work.loc[loc_series, 'claim_calc_type'] = '4x_principal'

    """
    с 01.01.2017 г  по 27.01.2019 г. (включительно)
    Общая сумма начисленных % не должна превышать трехкратный размер суммы займа (3 ОД) 
    за весь период. После выхода на просрочку % могут начисляться только на непогашенную  
    часть ОД и не должны превышать двукратный размер основного долга (2 ОД). 
    В случае гашений, кредитор может доначислить % , но общая сумма начисленных % 
    не должна превышать 3 ОД за весь период.
    """

    # 3ОД
    date_cutoff_bottom = pd.Timestamp('2017-01-01')
    date_cutoff_top = pd.Timestamp('2019-01-27')
    loc_series = (
            (df_work['credit_date_created'] <= date_cutoff_top)
            & (df_work['credit_date_created'] >= date_cutoff_bottom)
            & (df_work['credit_sum'] == df_work['credit_sum'])
    )

    df_work['3x_principal'] = df_work['credit_sum'] * 3

    df_work.loc[loc_series, 'claim_interest'] = df_work.loc[
        loc_series, ['3x_principal', 'debt_interest']
    ].min(axis='columns')
    df_work.loc[loc_series, 'claim_calc_type'] = '3x_principal'

    # 2ОД
    loc_series = (
            (df_work['credit_date_created'] <= date_cutoff_top)
            & (df_work['credit_date_created'] >= date_cutoff_bottom)
            & (df_work['credit_sum'] != df_work['credit_sum'])
    )

    df_work['2x_principal'] = df_work['debt_principal'] * 2

    df_work.loc[loc_series, 'claim_interest'] = df_work.loc[
        loc_series, ['2x_principal', 'debt_interest']
    ].min(axis='columns')
    df_work.loc[loc_series, 'claim_calc_type'] = '2x_principal'

    """
    с 28.01.2019 г. по  30.06.2019 г. (включительно)
    сумма всех начислений (%, неустойки, иных платежей) не должна превышать 2,5 размеров суммы займа (2,5 ОД);
    % ставка не может превышать 1,5 % в день. 

    Не для займов до 10000 руб, сроком до 15 дней  
    """
    date_cutoff_bottom = pd.Timestamp('2019-01-28')
    date_cutoff_top = pd.Timestamp('2019-06-30')
    loc_series = (
            (df_work['credit_date_created'] <= date_cutoff_top)
            & (df_work['credit_date_created'] >= date_cutoff_bottom)
            & ~((df_work['credit_sum'] <= 10000) & (df_work['credit_term_days'] <= 15))
    )
    df_work['2.5x_principal'] = df_work['credit_sum'] * 2.5
    df_work['1.5%day_max'] = (
            df_work['credit_sum']
            * (1.5 / 100)
            * ((pd.Timestamp.now().floor('1D') - df_work['credit_date_created']) / pd.Timedelta('1 day'))
    )

    df_work.loc[loc_series, 'claim_interest'] = df_work.loc[
        loc_series, ['2.5x_principal', 'debt_interest', '1.5%day_max']
    ].min(axis='columns')
    df_work.loc[loc_series, 'claim_calc_type'] = '2.5x_principal/1.5%day_max'

    """
    Для займов до 10 000 руб., сроком до 15 дней (включительно)
    размер начисленных %, комиссий и иных платежей, за исключением неустойки (штрафы, пени)
    не может превышать 30% от суммы займа. Неустойка может начисляться отдельно, в размере 0,1%
    от суммы просроченной задолженности за каждый день просрочки, после того, 
    как фиксируемая сумма платежей достигнет 30 процентов от суммы займа. 
    """
    loc_series = (
            (df_work['credit_date_created'] <= date_cutoff_top)
            & (df_work['credit_date_created'] >= date_cutoff_bottom)
            & ((df_work['credit_sum'] <= 10000) & (df_work['credit_term_days'] <= 15))
    )

    # привожу ставку к 1.5% в день
    df_work.loc[loc_series, 'credit_rate'] = df_work.loc[loc_series, 'credit_rate'].apply(
        lambda x: x if x <= (1.5 / 100) * 365 else (1.5 / 100) * 365
    )

    df_work['0.3x_principal'] = df_work['credit_sum'] * 0.3
    df_work['date_after_0.3x_principal'] = (
                                                   df_work['0.3x_principal']
                                                   / (df_work['credit_sum'] * (df_work['credit_rate'] / 365))
                                           ) * pd.Timedelta('1 day') + df_work['credit_date_created']

    df_work['date_after_0.3x_principal'] = df_work['date_after_0.3x_principal'].apply(
        lambda x: x if x < pd.Timestamp.now() else pd.Timestamp.now()
    ).dt.floor('1D')

    df_work['0.3x_principal+penalty'] = (
            df_work['0.3x_principal']
            + (
                    (pd.Timestamp.now().floor('1D') - df_work['date_after_0.3x_principal'])
                    / pd.Timedelta('1 day')
            ) * df_work['debt_principal'] * 0.001
    )

    df_work.loc[loc_series, 'claim_interest'] = df_work.loc[
        loc_series, ['0.3x_principal+penalty', 'debt_interest']
    ].min(axis='columns')
    df_work.loc[loc_series, 'claim_calc_type'] = '0.3x_principal+0.1%day_penalty'

    """
    с 01.07.2019 г. по 31.12.2019 г. (включительно) 
    сумма всех начислений (%, неустойки, иных платежей) не должна превышать
    двукратный размер суммы займа (2 ОД) (независимо от того, были ли гашения по ОД); 
    """
    date_cutoff_bottom = pd.Timestamp('2019-07-01')
    date_cutoff_top = pd.Timestamp('2019-12-31')
    loc_series = (
            (df_work['credit_date_created'] <= date_cutoff_top)
            & (df_work['credit_date_created'] >= date_cutoff_bottom)
    )

    df_work['2x_principal'] = df_work['credit_sum'] * 2

    df_work.loc[loc_series, 'claim_interest'] = df_work.loc[
        loc_series, ['2x_principal', 'debt_interest']
    ].min(axis='columns')
    df_work.loc[loc_series, 'claim_calc_type'] = '2x_principal'

    """
    с 01.01.2020 г. (включительно)
    сумма всех начислений (%, неустойки, иных платежей) не должна превышать 
    полуторакратный размер суммы займа (1,5 ОД) (независимо от того, были ли гашения по ОД). 
    """

    date_cutoff_bottom = pd.Timestamp('2020-01-01')
    date_cutoff_top = pd.Timestamp('2099-12-31')
    loc_series = (
            (df_work['credit_date_created'] <= date_cutoff_top)
            & (df_work['credit_date_created'] >= date_cutoff_bottom)
    )

    df_work['1.5x_principal'] = df_work['credit_sum'] * 1.5

    df_work.loc[loc_series, 'claim_interest'] = df_work.loc[
        loc_series, ['1.5x_principal', 'debt_interest']
    ].min(axis='columns')
    df_work.loc[loc_series, 'claim_calc_type'] = '1.5x_principal'

    df_work['claim_sum'] = df_work['debt_principal'] + df_work['claim_interest']

    return df_work[['claim_sum', 'claim_interest', 'claim_calc_type']]
